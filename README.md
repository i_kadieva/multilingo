# Team 11


## Multilingo Translation Portal

### Description

MultiLingo is text translation app which could be used to show the content of a web site (for example a knowledge sharing portal/wiki) in multiple languages.


###Fist step
   - Files you should add at ROOT LEVEL
        * .env 
        ``` 
          PORT=3000
          DB_TYPE=mysql
          DB_HOST=localhost
          DB_PORT=3306
          DB_USERNAME=root
          DB_PASSWORD={your password}
          DB_DATABASE_NAME={your databese name}
          JWT_SECRET={your jwt-secret}
          JWT_EXPIRE_TIME=3600
          GOOGLE_APPLICATION_CREDENTIALS= {{path to json file with information from google.cloud}}
     ```
      * ormconfig.json
      ``` 
          {
              "type": "mysql",
              "host": "localhost",
              "port": 3306,
              "username": "root",
              "password": {your password},
              "database": {your databese name},
              "synchronize": true,
              "logging": false,
              "entities": [
                "src/database/entities/**/*.ts"
              ],
              "migrations": [
                "src/database/migrations/**/*.ts"
              ],
              "cli": {
                "entitiesDir": "src/database/entities",
                "migrationsDir": "src/database/migrations"
              }
          }
        ```  
     * {file from google.translation.cloud}.json
     
           
               {
                 "type": "**",
                 "project_id": "**",
                 "private_key_id": "**",
                 "private_key": "**",
                 "client_email": "**",
                 "client_id": "**",
                 "auth_uri": "**",
                 "token_uri": "**",
                 "auth_provider_x509_cert_url": "**",
                 "client_x509_cert_url": "**"
               }
      
   - Files you should add at server/src/database/
        * ormconfig.ts
             
                     import { ConnectionOptions } from 'typeorm';
                     
                     const config: ConnectionOptions = {
                         type: 'mysql',
                         host: 'localhost',
                         port: 3306,
                         username: 'root',
                         password: 'your password',
                         database: 'your databese name',
                         entities: [__dirname + '/**/*.entity{.ts,.js}'],
                     
                         synchronize: false,
                     
                         migrationsRun: true,
                     
                         migrations: [__dirname + '/**/migrations/**/*{.ts,.js}'],
                         cli: {
                             migrationsDir: 'src/database/migrations',
                         },
                     };
                     
                     export = config;
 
   - When setting you database you should choose 
        * Create scheme named (same name with your .env file)
        * Charset/Collation - utf8

## Server side

 * Installation

```bash
$ npm install
```

* Running the app

```bash
# development
$ npm run typeorm:run - to process migrations
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

* Seed

```bash
# seed for user, languages and roles
$ npm run seed
    
 To run this you !MUST! open second terminal write this command there 
# seed for mock articles and ui-componets
$ npm run ui-article-seed
```

* Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

### General guide

 - You can open Swagger for API calls
    * http://localhost:3000/multilingo/swagger/session
    * http://localhost:3000/multilingo/swagger/users
    * http://localhost:3000/multilingo/swagger/articles
    * http://localhost:3000/multilingo/swagger/uiElementstranslations
    * http://localhost:3000/multilingo/swagger/translations
    

### Client
  - You can clone this repo 'https://gitlab.com/Stoyan24/multilingo-front-end' and use this client    
