import { ShowRatingDTO } from './../../rating/models/show-rating.dto';
import { ApiModelProperty } from '@nestjs/swagger';
import { Publish } from './../../common/middleware/transformer/decorators/publish';

export class ShowRatedVersionDTO {

    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish()
    public imgPath: string;

    @ApiModelProperty()
    @Publish()
    public title: string;

    @ApiModelProperty()
    @Publish()
    public content: string;

    @ApiModelProperty()
    @Publish()
    public version: number;

    @ApiModelProperty()
    @Publish()
    public isCurrent: boolean;

    @ApiModelProperty()
    @Publish()
    public isDeleted?: boolean;

    @ApiModelProperty()
    @Publish()
    public language?: string;

    @ApiModelProperty()
    @Publish(ShowRatingDTO)
    public titleRating: ShowRatingDTO;

    @ApiModelProperty()
    @Publish(ShowRatingDTO)
    public contentRating: ShowRatingDTO;

}
