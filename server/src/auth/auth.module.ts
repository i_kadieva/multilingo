import {Blacklist} from './../database/entities/blacklist.entity';
import {Module} from '@nestjs/common';
import {AuthController} from './auth.controller';
import {AuthService} from './auth.service';
import {JwtModule} from '@nestjs/jwt';
import {DatabaseModule} from '../database/database.module';
import {ConfigModule} from '../config/config.module';
import {ConfigService} from '../config/config.service';
import {JwtStrategy} from './strategy/jwt.strategy';
import {UsersModule} from '../users/users.module';
import {TypeOrmModule} from '@nestjs/typeorm';
import {PassportModule} from '@nestjs/passport';
import {Language} from '../database/entities/language.entity';

@Module({
    imports: [
        PassportModule,
        ConfigModule,
        DatabaseModule,
        UsersModule,
        TypeOrmModule.forFeature([Blacklist, Language]),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.jwtSecret,
                signOptions: {
                    expiresIn: configService.jwtExpireTime,
                },
            }),
        }),
    ],
    providers: [AuthService, JwtStrategy],
    controllers: [AuthController],
    exports: [AuthService],
})
export class AuthModule {
}
