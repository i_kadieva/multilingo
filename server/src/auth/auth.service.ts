import { Blacklist } from './../database/entities/blacklist.entity';
import { HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Role } from '../database/entities/role.entity';
import { MultiLingoSystemError } from '../common/middleware';
import { User } from '../database/entities/user.entity';
import { UsersService } from '../users/users.service';
import { LoginDto } from './models/login.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Language } from '../database/entities/language.entity';
import { IPayloadToken } from './models/IPayloadToken';

@Injectable()
export class AuthService {

    public constructor(
        @InjectRepository(Blacklist) private readonly blacklistRepository: Repository<Blacklist>,
        @InjectRepository(Language) private readonly languageRepository: Repository<Language>,
        private readonly userDataService: UsersService,
        private readonly jwtService: JwtService,
    ) {}

    public async login(user: LoginDto): Promise<any> {
        const foundUser: User = await this.userDataService.findUserByUsername(user.usernameOrEmail);

        if (!foundUser) {
            throw new MultiLingoSystemError('Unauthorized', HttpStatus.UNAUTHORIZED);
        }
        if (!(await this.userDataService.validateUserPassword(user))) {
            throw new MultiLingoSystemError('Invalid credentials!', HttpStatus.BAD_REQUEST);
        }

        const userPreferedLang: Language = await this.languageRepository.findOne({where: {code: foundUser.preferedLangCode}});

        const payload: IPayloadToken = {
            username: foundUser.username,
            id: foundUser.id,
            email: foundUser.email,
            roles: foundUser.roles.map((role: Role) => role.name),
            preferedLangCode: { name: userPreferedLang.name, code: userPreferedLang.code},
            avatarPath: foundUser.avatarPath,
        };

        return {
            token: await this.jwtService.signAsync(payload),
        };
    }

    public async blacklistToken(token: string) {
        const tockenEntity: Blacklist = this.blacklistRepository.create({ token });
        let savedtocken: Blacklist;
        try {
            savedtocken = await this.blacklistRepository.save(tockenEntity);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    public async isTokenBlacklisted(token: string): Promise<boolean> {
        let blacklist: Blacklist;
        try {
            blacklist = await this.blacklistRepository.findOne({ token });
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }

        return blacklist ? true : false;
    }
}
