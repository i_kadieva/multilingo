import {HttpStatus, Injectable} from '@nestjs/common';
import {PassportStrategy} from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import {ConfigService} from '../../config/config.service';
import {UsersService} from '../../users/users.service';
import {IPayload} from '../models/IPayload';
import {MultiLingoSystemError} from '../../common/middleware';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly userDataService: UsersService,
    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.jwtSecret,
      ignoreExpiration: false,
    });
  }

  public async validate(payload: IPayload): Promise<any> {
    const user = await this.userDataService.findUserByUsername(
      payload.username,
    );

    if (!user) {
      throw new MultiLingoSystemError('Unauthorized', HttpStatus.UNAUTHORIZED);
    }

    return user;
  }
}
