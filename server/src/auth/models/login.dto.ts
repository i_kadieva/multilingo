import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length, Matches } from 'class-validator';

export class LoginDto {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    @Length(2, 20)
    public usernameOrEmail: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, {
        message:
            'The password must be minimum eight characters, one letter and one number',
    })
    public password: string;
}
