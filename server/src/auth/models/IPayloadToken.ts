export interface IPayloadToken {
    username: string;
    id: string;
    email: string;
    roles?: string[];
    preferedLangCode: { name: string, code: string};
    avatarPath: string;
}
