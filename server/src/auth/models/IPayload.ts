import { Role } from '../../database/entities/role.entity';

export interface IPayload {
    username: string;
    id: string;
    email: string;
    roles?: Role[];
}
