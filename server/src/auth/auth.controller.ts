import {Body, Controller, Delete, HttpCode, HttpStatus, Post, UseGuards} from '@nestjs/common';
import {AuthService} from './auth.service';
import {ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {LoginDto} from './models/login.dto';
import {Token} from '../common/middleware/decorators/token.decorator';

@ApiUseTags('session')
@Controller('multilingo/session')
export class AuthController {
    public constructor(private readonly authService: AuthService) {}

    @Post()
    @HttpCode(HttpStatus.OK)
    @ApiCreatedResponse({description: 'Access granted!'})
    @ApiBadRequestResponse({description: 'Access denied!'})
    @ApiBearerAuth()
    public async loginUser(@Body() user: LoginDto) {

        return await this.authService.login(user);
    }

    @Delete()
    @UseGuards(AuthGuard('jwt'))
    @HttpCode(HttpStatus.OK)
    @ApiCreatedResponse({description: 'Logged out!'})
    @ApiBadRequestResponse({description: 'Cannot be logged out!'})
    @ApiBearerAuth()
    public async logoutUser(@Token() token: string) {
        await this.authService.blacklistToken(token);
        return {
            msg: 'Successful logout!',
        };
    }
}
