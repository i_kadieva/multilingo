import { CoreModule } from './common/core.module';
import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { DatabaseModule } from './database/database.module';
import { ConfigModule } from './config/config.module';
import { ArticlesModule } from './articles/articles.module';
import { LanguagesModule } from './languages/languages.module';
import { TranslationsModule } from './translations/translations.module';
import { UiComponentModule } from './ui-component/ui-component.module';
import {SeedModule} from './seed/seed.module';

@Module({
  imports: [
    ArticlesModule,
    AuthModule,
    ConfigModule,
    CoreModule,
    DatabaseModule,
    LanguagesModule,
    TranslationsModule,
    UsersModule,
    SeedModule,
    UiComponentModule,
  ],
})
export class AppModule {}
