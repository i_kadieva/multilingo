import {Module} from '@nestjs/common';
import {UsersService} from './users.service';
import {User} from '../database/entities/user.entity';
import {Role} from '../database/entities/role.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {DatabaseModule} from '../database/database.module';
import {UsersController} from './users.controller';

@Module({
    imports: [
        DatabaseModule,
        TypeOrmModule.forFeature([
            Role,
            User,
        ])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {
}
