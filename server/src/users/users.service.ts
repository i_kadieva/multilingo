import {HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {User} from '../database/entities/user.entity';
import {Repository} from 'typeorm';
import {MultiLingoSystemError} from '../common/middleware';
import {RoleType} from '../common/enums/role-type';
import {Role} from '../database/entities/role.entity';
import * as bcrypt from 'bcrypt';
import {CreateUserDTO} from './models/create-user.dto';
import {UpdateUserDto} from './models/update-user.dto';
import {LoginDto} from '../auth/models/login.dto';

@Injectable()
export class UsersService {
    public constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Role) private readonly roleRepository: Repository<Role>,
    ) {
    }

    public async createUser(user: CreateUserDTO, ...roles: RoleType[]): Promise<User> {

        const foundUser: User = await this.userRepository.findOne({
            username: user.username,
            isDeleted: false,
        });
        if (foundUser) {
            throw new MultiLingoSystemError('User with such username already exists!', HttpStatus.BAD_REQUEST);
        }

        const checkEmail: User = await this.userRepository.findOne({
            email: user.email,
            isDeleted: false,
        });
        if (checkEmail) {
            throw new MultiLingoSystemError('User with such email already exists!', HttpStatus.BAD_REQUEST);
        }

        const userAssignedRoles: Role[] = await Promise.all(roles.map(role =>
            this.roleRepository.findOne({name: RoleType[role]})));

        const userEntity: User = this.userRepository.create(user);
        userEntity.password = await bcrypt.hash(user.password, 10);
        userEntity.roles = userAssignedRoles;

        return await this.userRepository.save(userEntity);
    }

    public async updateUser(user: Partial<UpdateUserDto>): Promise<User> {

        const foundUser: User = await this.userRepository.findOne({
            username: user.username,
            isDeleted: false,
        });
        if (foundUser) {
            throw new MultiLingoSystemError('User with such username already exists!', HttpStatus.BAD_REQUEST);
        }

        const updatedUser = { ...foundUser, ...user };
        return await this.userRepository.save(updatedUser);
    }

    public async getAllUsers(): Promise<User[]> {
        return await this.userRepository.find({isDeleted: false});
    }

    public async getUserByUsername(username: string): Promise<User> {
        const foundUser: User = await this.userRepository.findOne({where: {username}, relations: ['articles']});
        if (!foundUser) {
            throw new MultiLingoSystemError('User with such username do not exists!', HttpStatus.BAD_REQUEST);
        }
        return foundUser;
    }

    public async deleteUser(userId: string): Promise<User> {

        const foundUser: User = await this.userRepository.findOne({
            where: {id: userId, isDeleted: false},
        });
        if (foundUser === undefined) {
            throw new MultiLingoSystemError(`A user with such id does not exists!`, HttpStatus.BAD_REQUEST);
        }
        foundUser.isDeleted = !foundUser.isDeleted;

        return await this.userRepository.save(foundUser);
    }

    public async addRole(userId: string, role: RoleType): Promise<User> {
        const foundUser = await this.userRepository.findOne({
            where: {id: userId},
        });
        if (foundUser === undefined) {
            throw new MultiLingoSystemError(`A user with such id does not exists!`, HttpStatus.BAD_REQUEST);
        }
        const roleAdd: Role = await this.roleRepository.findOne({where: {name: RoleType[role]}});
        const banStatus: boolean = foundUser.roles.some((r: Role) => r.name === RoleType[role]);
        if (banStatus) {
            foundUser.roles = foundUser.roles.filter((r: Role) => r.name !== RoleType[role]);
        } else {
            foundUser.roles.push(roleAdd);
        }
        await this.userRepository.save(foundUser);

        return foundUser;
    }

    public async findUserByUsername(usernameOrEmail: LoginDto | string): Promise<User> {
        const foundUser: User = await this.userRepository.findOne({
            where: [{
                username: usernameOrEmail,
                isDeleted: false,
            }, {
                email: usernameOrEmail,
                isDeleted: false,
            }],
        });

        if (foundUser === undefined) {
            throw new MultiLingoSystemError(`Wrong username or email!`, HttpStatus.BAD_REQUEST);
        }

        return foundUser;
    }

    public async findUserByUsernameForAuth(usernameOrEmail: LoginDto | string): Promise<User> {
        return await this.userRepository.findOne({
            where: [{
                username: usernameOrEmail,
                isDeleted: false,
            }, {
                email: usernameOrEmail,
                isDeleted: false,
            }],
        });
    }

    public async validateUserPassword(user: LoginDto): Promise<boolean> {
        const foundUser: User = await this.userRepository.findOne({
            where: [{
                username: user.usernameOrEmail,
                isDeleted: false,
            }, {
                email: user.usernameOrEmail,
                isDeleted: false,
            }],
        });

        return bcrypt.compare(user.password, foundUser.password);
    }

    public async getUserById(id: string, withDeleted: boolean) {
        let user: User;
        try {
            if (withDeleted) {
                const where = { id, isDeleted: withDeleted };
                user = await this.userRepository.findOne({ where });
            } else {
                user = await this.userRepository.findOne({ id });
            }
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return user;
    }

}
