import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Length, Matches } from 'class-validator';

export class CreateUserDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    @Length(2, 20)
    public username: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, {
        message:
        'The password must be minimum eight characters, one letter and one number',
    })
    public password: string;

    @ApiModelProperty()
    @IsEmail()
    @IsNotEmpty()
    @IsString()
    public email: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public avatarPath: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public preferedLangCode: string;
}
