import { ApiModelProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';
import { RoleType } from '../../common/enums/role-type';

export class UserRoleUpdateDto {
    @ApiModelProperty({
        enum: ['Administrator', 'Editor', 'Contributor'],
        example: RoleType.Contributor,
    })
    @IsNotEmpty()
    @IsEnum(RoleType)
    public name: RoleType;
}
