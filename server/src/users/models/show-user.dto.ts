import { ApiModelProperty } from '@nestjs/swagger';
import { Role} from '../../database/entities/role.entity';
import { Publish} from '../../common/middleware/transformer/decorators/publish';

export class ShowUserDTO {

    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish()
    public username: string;

    @ApiModelProperty()
    @Publish()
    public email: string;

    @ApiModelProperty()
    @Publish()
    public avatarPath: string;

    @ApiModelProperty()
    @Publish()
    public preferedLangCode: string;

    @ApiModelProperty()
    @Publish()
    public roles: Role[];
}
