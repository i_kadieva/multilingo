import {
    Body,
    Controller, Delete, Get,
    HttpCode,
    HttpStatus, Param,
    Post,
    Put,
    UseGuards,
    UseInterceptors,
    ValidationPipe,
} from '@nestjs/common';
import {TransformInterceptor} from '../common/middleware/transformer/interceptors/transform.interceptor';
import {ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiOkResponse} from '@nestjs/swagger';
import {UsersService} from './users.service';
import {RoleType} from '../common/enums/role-type';
import {CreateUserDTO} from './models/create-user.dto';
import {UpdateUserDto} from './models/update-user.dto';
import {RoleGuard, Roles} from '../common/middleware';
import {AuthGuard} from '@nestjs/passport';
import {User} from '../database/entities/user.entity';
import {UserRoleUpdateDto} from './models/user-role-update.dto';
import {ShowUserDTO} from './models/show-user.dto';

@Controller('multilingo/users')
export class UsersController {

    public constructor(
        private readonly userDataService: UsersService,
    ) {}

    @Post()
    @UseInterceptors(new TransformInterceptor(ShowUserDTO))
    @ApiCreatedResponse({description: 'A new user was created!', type: ShowUserDTO})
    @ApiBadRequestResponse({description: 'The user was not created!'})
    @ApiBearerAuth()
    @HttpCode(HttpStatus.CREATED)
    public async addUser(@Body(new ValidationPipe()) body: CreateUserDTO) {
        return await this.userDataService.createUser(body, RoleType.Contributor);
    }

    @Get()
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @UseInterceptors(new TransformInterceptor(ShowUserDTO))
    @ApiOkResponse({ description: 'Returns all users!', type: User })
    @ApiBadRequestResponse({ description: 'No books!' })
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    public async getUsers() {

        return await this.userDataService.getAllUsers();
    }

    @Get(':username')
    @UseInterceptors(new TransformInterceptor(ShowUserDTO))
    @ApiOkResponse({ description: 'Returns user if exists!', type: 'string' })
    @ApiBadRequestResponse({ description: 'No such user!' })
    @HttpCode(HttpStatus.OK)
    public async getUserByUsername(@Param('username') username: string) {
        return await this.userDataService.getUserByUsername(username);
    }

    @Put()
    @UseInterceptors(new TransformInterceptor(ShowUserDTO))
    @ApiCreatedResponse({description: 'A new user was created!', type: ShowUserDTO})
    @ApiBadRequestResponse({description: 'The user was not created!'})
    @ApiBearerAuth()
    @HttpCode(HttpStatus.CREATED)
    public async updateUser(@Body(new ValidationPipe()) body: UpdateUserDto) {

        return await this.userDataService.updateUser(body);
    }

    @Delete(':userId')
    @Roles(RoleType.Administrator)
    @UseInterceptors(new TransformInterceptor(ShowUserDTO))
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @ApiOkResponse({description: 'User was deleted by user/admin', type: ShowUserDTO})
    @ApiBadRequestResponse({description: 'The user was not deleted!'})
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    public async deleteUser(
        @Param('userId') userId: string,
    ): Promise<User> {

        return await this.userDataService.deleteUser(userId);
    }

    @Put(':id/role')
    @HttpCode(HttpStatus.CREATED)
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @UseInterceptors(new TransformInterceptor(ShowUserDTO))
    @ApiCreatedResponse({description: 'User role was added', type: User})
    @ApiBadRequestResponse({description: 'A user with such id does not exists!!'})
    @ApiBearerAuth()
    public async addRole(
        @Param('id') id: string,
        @Body(new ValidationPipe({transform: true, whitelist: true})) role: UserRoleUpdateDto,
        ) {

        return await this.userDataService.addRole(id, role.name);
    }
}
