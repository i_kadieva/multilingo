import { RoleType } from './../common/enums/role-type';
import { Injectable } from '@nestjs/common';

@Injectable()
export class HelpperService {

    public isAdmin(roles: string[]): boolean {
        if (roles.find((role: string) => role === RoleType.Administrator)) {
            return true;
        }
        return false;
    }
}
