import { Module } from '@nestjs/common';
import { HelpperService } from './helpper.service';

@Module({
    providers: [HelpperService],
    exports: [HelpperService],
})
export class HelpperModule {}
