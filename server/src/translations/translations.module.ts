import { TypeOrmModule } from '@nestjs/typeorm';
import { Language } from './../database/entities/language.entity';
import { Module, forwardRef } from '@nestjs/common';
import { Rating } from './../database/entities/rating.entity';
import { Translation } from './../database/entities/translation.entity';
import { TranslationsService } from './translations.service';
import { TranslationsController } from './translations.controller';
import { LanguagesModule } from './../languages/languages.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Language, Translation, Rating]),
    forwardRef(() => LanguagesModule),
  ],
  providers: [TranslationsService],
  controllers: [TranslationsController],
  exports: [TranslationsService],
})
export class TranslationsModule {}
