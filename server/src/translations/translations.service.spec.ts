import { TranslationDTO } from './models/translation.dto';
import { CreateTranslationDTO } from './models/create-translation.dto';
import { Rating } from './../database/entities/rating.entity';
import { Translation } from './../database/entities/translation.entity';
import { LanguagesService } from './../languages/languages.service';
import { Test, TestingModule } from '@nestjs/testing';
import { TranslationsService } from './translations.service';
import { ArticleTranslationDTO } from './models/article-translation.dto';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Language } from './../database/entities/language.entity';

describe('TranslationsService', () => {
  let service: TranslationsService;

  let languagesService: any;
  let ratingsRepo: any;
  let translationsRepo: any;

  beforeEach(async () => {
    languagesService = {
      getAllLanguage() {
        /* empty */
      },
    };

    ratingsRepo = {
      findOne() {
        /* empty */
      },
      create() {
        /* empty */
      },
      save() {
        /* empty */
      },
    };

    translationsRepo = {
      find() {
        /* empty */
      },
      findOne() {
        /* empty */
      },
      create() {
        /* empty */
      },
      save() {
        /* empty */
      },
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TranslationsService,
        { provide: LanguagesService, useValue: languagesService },
        { provide: getRepositoryToken(Translation), useValue: translationsRepo },
        { provide: getRepositoryToken(Rating), useValue: ratingsRepo },
      ],
    }).compile();

    service = module.get<TranslationsService>(TranslationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllTranslations()', () => {
    it('should call translationsRepository find() once with filtering object', async () => {
      // Arrange
      const fakeCriteria = { targetLanguageCode: 'en' };
      const spy = jest
        .spyOn(translationsRepo, 'find').mockImplementation()
        .mockReturnValue(Promise.resolve([]));

      // Act
      const result = await service.getAllTranslations(fakeCriteria);

      // Assert
      expect(spy).toBeCalledWith({
        where: fakeCriteria,
        relations: ['rating'],
      });
      expect(spy).toBeCalledTimes(1);
      spy.mockRestore();
    });

    it('should return an array of translations', async () => {
      // Arrange
      const fakeCriteria = { targetLanguageCode: 'en' };
      const fakeTranslation: Translation = new Translation();

      const spy = jest
        .spyOn(translationsRepo, 'find')
        .mockReturnValue(Promise.resolve([fakeTranslation]));

      // Act
      const result = await service.getAllTranslations(fakeCriteria);

      // Assert
      expect(result.length).toEqual(1);
      expect(result[0]).toBeInstanceOf(Translation);
      expect(result[0]).toEqual(fakeTranslation);
      spy.mockRestore();
    });

    // it('should throw if translations repository throw an error', async () => {
    //   // Arrange
    //   const fakeCriteria = { targetLanguageCode: 'en' };
    //   const spy = jest
    //     .spyOn(translationsRepo, 'find')
    //     .mockReturnValue(Promise.resolve([]));

    //   // Act
    //   const result = await service.getAllTranslations(fakeCriteria);

    //   // Assert
    //   expect(spy).toBeCalledWith(fakeCriteria);
    //   expect(spy).toBeCalledTimes(1);
    // });
    // spy.mockRestore();
  });

  describe('getAllTranslations()', () => {
    it('should call translationsRepository find() once with filtering object', async () => {
      // Arrange
      const fakeCriteria = { targetLanguageCode: 'en' };
      const spy = jest
        .spyOn(translationsRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      // Act
      const result = await service.getAllTranslations(fakeCriteria);

      // Assert
      expect(spy).toBeCalledWith({
        where: fakeCriteria,
        relations: ['rating'],
      });
      expect(spy).toBeCalledTimes(1);
      spy.mockRestore();
    });

    it('should return transformed translation for article', async () => {
      // Arrange
      const fakeCriteria = {
        targetLanguageCode: 'en',
        originalText: 'original',
      };
      const fakeTranslation: Translation = {
        id: '1',
        translation: 'text',
        originalText: 'original',
        originalLanguageCode: 'en',
        targetLanguageCode: 'es',
        isDeleted: false,
        rating: {
          id: '1',
          voted: 5,
          rating: 25,
          avgrating: 5,
        },
      };
      const transformedTranslation: ArticleTranslationDTO = {
        originalText: 'original',
        translatedText: 'text',
        rating: {
          id: '1',
          avgrating: 5,
        },
      };

      const spy = jest
        .spyOn(service, 'getTranslation')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      const result = await service.getTranslationForArticle(fakeCriteria);

      // Assert
      expect(result).toEqual(transformedTranslation);
      spy.mockRestore();
    });
  });

  describe('getTranslation()', () => {
    it('should call translationsRepository findOne() once with correct filtering object if id was specified', async () => {
      // Arrange
      const fakeCriteria = {
        targetLanguageCode: 'en',
        id: '1',
      };
      const fakeId = '1';
      const fakeTranslation: Translation = {
        id: '1',
        translation: 'text',
        originalText: 'original',
        originalLanguageCode: 'en',
        targetLanguageCode: 'es',
        isDeleted: false,
        rating: null,
      };
      const spy = jest
        .spyOn(translationsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      const result = await service.getTranslation(fakeId, fakeCriteria);

      // Assert
      expect(spy).toBeCalledWith({
        where: fakeCriteria,
        relations: ['rating'],
      });
      expect(spy).toBeCalledTimes(1);
      spy.mockRestore();
    });

    it(`should call translationsRepository findOne() once
      with correct filtering object if id was not specified`, async () => {
      // Arrange
      const fakeCriteria = {
        targetLanguageCode: 'en',
      };
      const fakeId = undefined;
      const fakeTranslation: Translation = new Translation();
      const spy = jest
        .spyOn(translationsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      const result = await service.getTranslation(fakeId, fakeCriteria);

      // Assert
      expect(spy).toBeCalledWith({
        where: fakeCriteria,
        relations: ['rating'],
      });
      expect(spy).toBeCalledTimes(1);
      spy.mockRestore();
    });

    it('should return translation', async () => {
      // Arrange
      const fakeCriteria = {
        targetLanguageCode: 'en',
      };
      const fakeId = '1';
      const fakeTranslation: Translation = new Translation();

      const spy = jest
        .spyOn(translationsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      const result = await service.getTranslation(fakeId, fakeCriteria);

      // Assert
      expect(result).toBeInstanceOf(Translation);
      expect(result).toEqual(fakeTranslation);
      spy.mockRestore();
    });
  });

  describe('createTranslation()', () => {
    it('should call languagesRepository getAllLanguage() once', async () => {
      // Arrange
      const fakeTranslation: Translation = new Translation();
      const fakeLanguage: Language = new Language();
      const fakeRating: Rating = new Rating();
      const fakeBody: CreateTranslationDTO = new CreateTranslationDTO();

      const createTranslationSpy = jest
        .spyOn(translationsRepo, 'create')
        .mockReturnValue(fakeTranslation);

      const createRatingSpy = jest
        .spyOn(ratingsRepo, 'create')
        .mockReturnValue(fakeRating);

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const getAllLanguagesSpy = jest
        .spyOn(languagesService, 'getAllLanguage')
        .mockReturnValue(Promise.resolve([fakeLanguage]));

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const translateTextSpy = jest
        .spyOn(service, 'translateText')
        .mockReturnValue(Promise.resolve('translated text'));

      // Act
      const result = await service.createTranslation(fakeBody);

      // Assert
      expect(getAllLanguagesSpy).toBeCalledTimes(1);

      createRatingSpy.mockRestore();
      createTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      getAllLanguagesSpy.mockRestore();
      saveRatingSpy.mockRestore();
      translateTextSpy.mockRestore();
    });

    it('should call translateText() once with correct criteria object', async () => {
      // Arrange
      const fakeTranslation: Translation = new Translation();
      const fakeLanguage: Language = {
        id: '1',
        name: 'English',
        code: 'en',
        isActive: true,
      };
      const fakeRating: Rating = new Rating();
      const fakeBody: CreateTranslationDTO = {
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: '',
      };
      const fakecriteria: CreateTranslationDTO = { ...fakeBody, targetLanguageCode: 'en' };

      const createTranslationSpy = jest
        .spyOn(translationsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const createRatingSpy = jest
        .spyOn(ratingsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeRating));

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const getAllLanguagesSpy = jest
        .spyOn(languagesService, 'getAllLanguage')
        .mockReturnValue(Promise.resolve([fakeLanguage]));

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeRating));

      const translateTextSpy = jest
        .spyOn(service, 'translateText')
        .mockReturnValue(Promise.resolve(['translated text']));

      // Act
      await service.createTranslation(fakeBody);

      // Assert
      expect(translateTextSpy).toBeCalledWith(fakecriteria);
      expect(translateTextSpy).toBeCalledTimes(1);

      createRatingSpy.mockRestore();
      createTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      getAllLanguagesSpy.mockRestore();
      saveRatingSpy.mockRestore();
      translateTextSpy.mockRestore();
    });

    it('should throw error if no translation was made', async () => {
      // Arrange
      const fakeTranslation: Translation = new Translation();
      const fakeRating: Rating = new Rating();
      const fakeBody: CreateTranslationDTO = new CreateTranslationDTO();

      const createTranslationSpy = jest
        .spyOn(translationsRepo, 'create')
        .mockReturnValue(fakeTranslation);

      const createRatingSpy = jest
        .spyOn(ratingsRepo, 'create')
        .mockReturnValue(fakeRating);

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const getAllLanguagesSpy = jest
        .spyOn(languagesService, 'getAllLanguage')
        .mockReturnValue(Promise.resolve([]));

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const translateTextSpy = jest
        .spyOn(service, 'translateText')
        .mockReturnValue(Promise.resolve(['translated text']));

      // Act && Assert
      expect(service.createTranslation(fakeBody)).rejects.toThrow();

      createRatingSpy.mockRestore();
      createTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      getAllLanguagesSpy.mockRestore();
      saveRatingSpy.mockRestore();
      translateTextSpy.mockRestore();
    });

    it('should call ratingsRepository create() once with correct parameters', async () => {
      // Arrange
      const fakeTranslation: Translation = new Translation();
      const fakeLanguage: Language = {
        id: '1',
        name: 'English',
        code: 'en',
        isActive: true,
      };
      const fakeRating: Rating = new Rating();
      const fakeBody: CreateTranslationDTO = {
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: '',
      };

      const createTranslationSpy = jest
        .spyOn(translationsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const createRatingSpy = jest
        .spyOn(ratingsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeRating));

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const getAllLanguagesSpy = jest
        .spyOn(languagesService, 'getAllLanguage')
        .mockReturnValue(Promise.resolve([fakeLanguage]));

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeRating));

      const translateTextSpy = jest
        .spyOn(service, 'translateText')
        .mockReturnValue(Promise.resolve(['translated text']));

      // Act
      await service.createTranslation(fakeBody);

      // Assert
      expect(createRatingSpy).toBeCalledTimes(1);

      createRatingSpy.mockRestore();
      createTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      getAllLanguagesSpy.mockRestore();
      saveRatingSpy.mockRestore();
      translateTextSpy.mockRestore();
    });

    it('should call ratingsRepository save() once with correct parameters', async () => {
      // Arrange
      const fakeTranslation: Translation = new Translation();
      const fakeLanguage: Language = {
        id: '1',
        name: 'English',
        code: 'en',
        isActive: true,
      };
      const fakeRating: Rating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgrating: 5,
      };
      const fakeBody: CreateTranslationDTO = {
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: '',
      };

      const createTranslationSpy = jest
        .spyOn(translationsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const createRatingSpy = jest
        .spyOn(ratingsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeRating));

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const getAllLanguagesSpy = jest
        .spyOn(languagesService, 'getAllLanguage')
        .mockReturnValue(Promise.resolve([fakeLanguage]));

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeRating));

      const translateTextSpy = jest
        .spyOn(service, 'translateText')
        .mockReturnValue(Promise.resolve(['translated text']));

      // Act
      await service.createTranslation(fakeBody);

      // Assert
      expect(saveRatingSpy).toBeCalledTimes(1);
      expect(saveRatingSpy).toBeCalledWith(Promise.resolve(fakeRating));

      createRatingSpy.mockRestore();
      createTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      getAllLanguagesSpy.mockRestore();
      saveRatingSpy.mockRestore();
      translateTextSpy.mockRestore();
    });

    it('should call translationsRepository create() once with correct parameters', async () => {
      // Arrange
      const fakeTranslation: Translation = new Translation();
      const fakeLanguage: Language = {
        id: '1',
        name: 'English',
        code: 'en',
        isActive: true,
      };
      const fakeRating: Rating = new Rating();
      const fakeBody: CreateTranslationDTO = {
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: '',
      };
      const fakeCreateTranslation: TranslationDTO = {
        originalText: 'text',
        originalLanguageCode: 'bg',
        translation: 'translated text',
        targetLanguageCode: 'en',
        rating: fakeRating,
      };

      const createTranslationSpy = jest
        .spyOn(translationsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const createRatingSpy = jest
        .spyOn(ratingsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeRating));

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const getAllLanguagesSpy = jest
        .spyOn(languagesService, 'getAllLanguage')
        .mockReturnValue(Promise.resolve([fakeLanguage]));

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeRating));

      const translateTextSpy = jest
        .spyOn(service, 'translateText')
        .mockReturnValue(Promise.resolve('translated text'));

      // Act
      await service.createTranslation(fakeBody);

      // Assert
      expect(createTranslationSpy).toBeCalledTimes(1);
      expect(createTranslationSpy).toBeCalledWith(fakeCreateTranslation);

      createRatingSpy.mockRestore();
      createTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      getAllLanguagesSpy.mockRestore();
      saveRatingSpy.mockRestore();
      translateTextSpy.mockRestore();
    });

    it('should call translationsRepository save() once with correct parameters', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgrating: 5,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeLanguage: Language = {
        id: '1',
        name: 'English',
        code: 'en',
        isActive: true,
      };
      const fakeBody: CreateTranslationDTO = {
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: '',
      };

      const createTranslationSpy = jest
        .spyOn(translationsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const createRatingSpy = jest
        .spyOn(ratingsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeRating));

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const getAllLanguagesSpy = jest
        .spyOn(languagesService, 'getAllLanguage')
        .mockReturnValue(Promise.resolve([fakeLanguage]));

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeRating));

      const translateTextSpy = jest
        .spyOn(service, 'translateText')
        .mockReturnValue(Promise.resolve('translated text'));

      // Act
      await service.createTranslation(fakeBody);

      // Assert
      expect(saveRatingSpy).toBeCalledTimes(1);
      expect(saveRatingSpy).toBeCalledWith(Promise.resolve(fakeRating));

      createRatingSpy.mockRestore();
      createTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      getAllLanguagesSpy.mockRestore();
      saveRatingSpy.mockRestore();
      translateTextSpy.mockRestore();
    });

    it('should return array of translations object', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgrating: 5,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeLanguage: Language = {
        id: '1',
        name: 'English',
        code: 'en',
        isActive: true,
      };
      const fakeBody: CreateTranslationDTO = {
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: '',
      };
      const fakeTranslations = [fakeTranslation];

      const createTranslationSpy = jest
        .spyOn(translationsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const createRatingSpy = jest
        .spyOn(ratingsRepo, 'create')
        .mockReturnValue(Promise.resolve(fakeRating));

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      const getAllLanguagesSpy = jest
        .spyOn(languagesService, 'getAllLanguage')
        .mockReturnValue(Promise.resolve([fakeLanguage]));

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(Promise.resolve(fakeRating));

      const translateTextSpy = jest
        .spyOn(service, 'translateText')
        .mockReturnValue(Promise.resolve('translated text'));

      // Act
      const result = await service.createTranslation(fakeBody);

      // Assert
      expect(result).toEqual(fakeTranslations);

      createRatingSpy.mockRestore();
      createTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      getAllLanguagesSpy.mockRestore();
      saveRatingSpy.mockRestore();
      translateTextSpy.mockRestore();
    });
  });

  describe('updateTranslation()', () => {
    it('should call getTranslation() once with the correct parameters', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgrating: 5,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeId = '1';

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const getTranslationSpy = jest
        .spyOn(service, 'getTranslation')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      await service.updateTranslation(fakeId, 'text to update');

      // Assert
      expect(getTranslationSpy).toBeCalledTimes(1);
      expect(getTranslationSpy).toBeCalledWith(fakeId, undefined);

      getTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      saveRatingSpy.mockRestore();
    });

    it('should throw error if no translation was found', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgrating: 5,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeId = '1';

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const getTranslationSpy = jest
        .spyOn(service, 'getTranslation')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act && Assert
      expect(service.updateTranslation(fakeId, 'text to update')).rejects.toThrow();

      getTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      saveRatingSpy.mockRestore();
    });

    it('should call ratingsRepository save() once with correct parameters', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgrating: 5,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeId = '1';

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const getTranslationSpy = jest
        .spyOn(service, 'getTranslation')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      await service.updateTranslation(fakeId, 'text to update');

      // Assert
      expect(saveRatingSpy).toBeCalledTimes(1);
      expect(saveRatingSpy).toBeCalledWith(fakeRating);

      getTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      saveRatingSpy.mockRestore();
    });

    it('should throw error if rating repository rejects request', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgrating: 5,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeId = '1';

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const getTranslationSpy = jest
        .spyOn(service, 'getTranslation')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      await service.updateTranslation(fakeId, 'text to update');

      // Assert
      expect(Promise.reject(saveRatingSpy)).rejects.toThrow();

      getTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      saveRatingSpy.mockRestore();
    });

    it('should call translationsRepository save() once with correct parameters', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgrating: 5,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeId = '1';

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const getTranslationSpy = jest
        .spyOn(service, 'getTranslation')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      await service.updateTranslation(fakeId, 'translated text');

      // Assert
      expect(saveTranslationSpy).toBeCalledTimes(1);
      expect(saveTranslationSpy).toBeCalledWith(fakeTranslation);

      getTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      saveRatingSpy.mockRestore();
    });

    it('should throw error if translation repository  rejects request', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 0,
        rating: 0,
        avgrating: 0,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeId = '1';

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const getTranslationSpy = jest
        .spyOn(service, 'getTranslation')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      await service.updateTranslation(fakeId, 'text to update');

      // Assert
      expect(Promise.reject(saveTranslationSpy)).rejects.toThrow();

      getTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      saveRatingSpy.mockRestore();
    });

    it('should return an updated translation object', async () => {
      // Arrange
      const fakeRating: Rating = {
        id: '1',
        voted: 0,
        rating: 0,
        avgrating: 0,
      };
      const fakeTranslation: Translation = {
        id: '1',
        originalText: 'text',
        originalLanguageCode: 'bg',
        targetLanguageCode: 'en',
        translation: 'translated text',
        isDeleted: false,
        rating: fakeRating,
      };
      const fakeId = '1';

      const saveTranslationSpy = jest
        .spyOn(translationsRepo, 'save')
        .mockReturnValue(fakeTranslation);

      const saveRatingSpy = jest
        .spyOn(ratingsRepo, 'save')
        .mockReturnValue(fakeRating);

      const getTranslationSpy = jest
        .spyOn(service, 'getTranslation')
        .mockReturnValue(Promise.resolve(fakeTranslation));

      // Act
      await service.updateTranslation(fakeId, 'text to update');

      // Act
      const result = await service.updateTranslation(fakeId, 'text to update');

      // Assert
      expect(result).toEqual(fakeTranslation);

      getTranslationSpy.mockRestore();
      saveTranslationSpy.mockRestore();
      saveRatingSpy.mockRestore();
    });
  });

});
