import { Test, TestingModule } from '@nestjs/testing';
import { TranslationsController } from './translations.controller';
import { TranslationsService } from './translations.service';

describe('Translations Controller', () => {
  let controller: TranslationsController;
  let translationsService: any;

  beforeEach(async () => {
    translationsService = {
      getAllTranslations() { /* empty */ },
      getTranslationForArticle() { /* empty */ },
      getTranslation() { /* empty */ },
      createTranslation() { /* empty */ },
      updateTranslation() { /* empty */ },
      changeTranslationDeleteStatus() { /* empty */ },
      rateTranslation() { /* empty */ },
      translateText() { /* empty */ },
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TranslationsController],
      providers: [
        {
          provide: TranslationsService,
          useValue: translationsService,
        },
      ],
    }).compile();

    controller = module.get<TranslationsController>(TranslationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getTranslations()', () => {
    it('should call translationsService getAllTranslations() once with correct parameters', async () => {
      // Arrange
      const fakelanguageCode = { targetLanguageCode: 'en' };
      const spy = jest.spyOn(translationsService, 'getAllTranslations');

      // Act
      await controller.getTranslations(fakelanguageCode);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakelanguageCode);
    });

    it('should return all translations', async () => {
      // Arrange
      const fakeTranslations = [{ translation: 'title' }, { translation: 'content' }];
      const fakelanguageCode = { targetLanguageCode: 'en' };
      const spy = jest.spyOn(translationsService, 'getAllTranslations')
        .mockImplementation(async () => fakeTranslations);

      // Act
      const result = await controller.getTranslations(fakelanguageCode);

      // Assert
      expect(result).toEqual(fakeTranslations);
      spy.mockRestore();
    });
  });

  describe('getTranslationById()', () => {
    it('should call translationsService getTranslation() once with correct parameters', async () => {
      // Arrange
      const fakeTranslationId = '1';
      const fakelanguageCode = { targetLanguageCode: 'en' };
      const spy = jest.spyOn(translationsService, 'getTranslation');

      // Act
      await controller.getTranslationById(
        fakeTranslationId,
        fakelanguageCode,
      );

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(
        fakeTranslationId,
        fakelanguageCode,
      );
    });

    it('should return a translation', async () => {
      // Arrange
      const fakeTranslation = { translation: 'title' };
      const fakeTranslationId = '1';
      const fakelanguageCode = { targetLanguageCode: 'en' };
      const spy = jest.spyOn(translationsService, 'getTranslation')
        .mockImplementation(async () => fakeTranslation);

      // Act
      const result = await controller.getTranslationById(
        fakeTranslationId,
        fakelanguageCode,
      );

      // Assert
      expect(result).toEqual(fakeTranslation);
      spy.mockRestore();
    });
  });

  describe('updateTranslation()', () => {
    it('should call translationsService updateTranslation() once with correct parameters', async () => {
      // Arrange
      const fakeTranslationId = '1';
      const fakeTranslationToUpdate = { translation: 'new translation' };

      const spy = jest.spyOn(translationsService, 'updateTranslation');

      // Act
      await controller.updateTranslation(fakeTranslationId, fakeTranslationToUpdate);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeTranslationId, fakeTranslationToUpdate.translation);
    });

    it('should return updated translation', async () => {
      // Arrange
      const fakeTranslation = { translation: 'new translation' };
      const fakeTranslationId = '1';
      const fakeTranslationToUpdate = { translation: 'new translation' };

      const spy = jest.spyOn(translationsService, 'updateTranslation')
        .mockImplementation(async () => (fakeTranslation));

      // Act
      const result = await controller.updateTranslation(fakeTranslationId, fakeTranslationToUpdate);

      // Assert
      expect(result).toEqual(fakeTranslation);
      spy.mockRestore();
    });
  });

  describe('deleteTranslation()', () => {
    it('should call translationsService changeTranslationDeleteStatus() once with correct parameters', async () => {
      // Arrange
      const fakeTranslationId = '1';
      const spy = jest.spyOn(translationsService, 'changeTranslationDeleteStatus');

      // Act
      await controller.deleteTranslation(fakeTranslationId);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeTranslationId);
    });

    it('should return deleted translation', async () => {
      // Arrange
      const fakeTranslationId = '1';
      const fakeTranslation = { translation: 'new translation' };
      const spy = jest.spyOn(translationsService, 'changeTranslationDeleteStatus')
        .mockImplementation(() => (fakeTranslation));

      // Act
      const result = await controller.deleteTranslation(fakeTranslationId);

      // Assert
      expect(result).toEqual(fakeTranslation);
      spy.mockRestore();
    });
  });

  describe('updateRating()', () => {
    it('should call translationsService rateTranslation() once with correct parameters', async () => {
      // Arrange
      const fakeTranslationId = '1';
      const fakeBody = { rating: 5 };
      const spy = jest.spyOn(translationsService, 'rateTranslation');

      // Act
      await controller.updateRating(fakeTranslationId, fakeBody);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeTranslationId, fakeBody.rating);
    });

    it('should return updated rating', async () => {
      // Arrange
      const fakeTranslationId = '1';
      const fakeBody = { rating: 5 };
      const fakeRating = {
        id: '1',
        voted: 5,
        rating: 25,
        avgRating: 5,
      };
      const spy = jest.spyOn(translationsService, 'rateTranslation')
        .mockImplementation(async () => (fakeRating));

      // Act
      const result = await controller.updateRating(fakeTranslationId, fakeBody);

      // Assert
      expect(result).toEqual(fakeRating);
      spy.mockRestore();
    });
  });

});
