import { Rating } from './../database/entities/rating.entity';
import { ShowRatingDTO } from './../rating/models/show-rating.dto';
import { UpdateTranslationDTO } from './models/update-translation.dto';
import { Translation } from '../database/entities/translation.entity';
import { RoleGuard, Roles} from '../common/middleware';
import { RoleType } from '../common/enums/role-type';
import { AuthGuard } from '@nestjs/passport';
import { ShowTranslationDTO } from './models/show-translation.dto';
import { TransformInterceptor } from '../common/middleware/transformer/interceptors/transform.interceptor';
import { TranslationsService } from './translations.service';
import {
    Controller,
    Get,
    UseInterceptors,
    HttpCode,
    HttpStatus,
    Query,
    UseGuards,
    Param,
    Put,
    Body,
    ValidationPipe,
    Delete,
} from '@nestjs/common';
import { ApiUseTags, ApiOkResponse, ApiBadRequestResponse, ApiBearerAuth } from '@nestjs/swagger';
import { RateTranslationDto } from './models/rate-translation.dto';
import { AuthGuardWithBlacklisting } from '../common/middleware/guards/blacklist.guard';

@ApiUseTags('translations')
@Controller('multilingo/translations')
export class TranslationsController {

    public constructor(private readonly translationsService: TranslationsService) { }

    @Get()
    @Roles(RoleType.Administrator, RoleType.Editor)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowTranslationDTO))
    @ApiOkResponse({ description: 'Returns all articles with their translations', type: ShowTranslationDTO })
    @ApiBadRequestResponse({ description: 'No translations!' })
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    public async getTranslations(@Query('skip') skip: number, @Query('take') take: number): Promise<Translation[]> {
        return await this.translationsService.getAllTranslations(undefined, take, skip);
    }

    @Get(':content')
    @Roles(RoleType.Administrator, RoleType.Editor)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowTranslationDTO))
    @ApiOkResponse({ description: 'Return a translations by its content', type: ShowTranslationDTO })
    @ApiBadRequestResponse({ description: 'No such translation!' })
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    public async getTranslationByContent(
        @Param('content') content: string,
    ): Promise<Translation[] | Translation> {
        return this.translationsService.getAllTranslationsByContent(content);
    }

    @Put(':translationId')
    @Roles(RoleType.Administrator, RoleType.Editor)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @HttpCode(HttpStatus.OK)
    @UseInterceptors(new TransformInterceptor(ShowTranslationDTO))
    @ApiBearerAuth()
    @ApiOkResponse({ description: 'Update translation by id', type: ShowTranslationDTO })
    @ApiBadRequestResponse({ description: 'No translation with such id!' })
    public async updateTranslation(
        @Param('translationId') translationId: string,
        @Body(new ValidationPipe({ transform: true, whitelist: true })) body: UpdateTranslationDTO,
    ): Promise<Translation> {
        return await this.translationsService.updateTranslation(translationId, body.translation);
    }

    @Delete(':translationId')
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @HttpCode(HttpStatus.OK)
    @UseInterceptors(new TransformInterceptor(ShowTranslationDTO))
    @ApiBearerAuth()
    @ApiOkResponse({ description: 'Delete translation by id', type: ShowTranslationDTO })
    @ApiBadRequestResponse({ description: 'No translation with such id!' })
    public async deleteTranslation(
        @Param('translationId') translationId: string,
    ): Promise<Translation> {
        return await this.translationsService.changeTranslationDeleteStatus(translationId);
    }

    @Put(':translationId/rate')
    @HttpCode(HttpStatus.OK)
    @UseInterceptors(new TransformInterceptor(ShowRatingDTO))
    @ApiOkResponse({ description: 'Rate translation', type: ShowRatingDTO })
    @ApiBadRequestResponse({ description: 'No translation with such id!' })
    public async updateRating(
        @Param('translationId') translationId: string,
        @Body(new ValidationPipe({ transform: true, whitelist: true })) body: RateTranslationDto,
    ): Promise<Rating> {
        return await this.translationsService.rateTranslation(translationId, body.rating);
    }
}
