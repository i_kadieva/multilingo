import { ArticleTranslationDTO } from './models/article-translation.dto';
import { LanguagesService } from './../languages/languages.service';
import { GetTranslationDTO } from './models/get-translation.dto';
import { CreateTranslationDTO } from './models/create-translation.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, HttpStatus, Inject, forwardRef } from '@nestjs/common';
import { MultiLingoSystemError } from './../common/middleware/exceptions/multiLingo-system.error';
import { Rating } from './../database/entities/rating.entity';
import {Like, Repository} from 'typeorm';
import { Translation } from './../database/entities/translation.entity';
import * as GoogleTranslate from '@google-cloud/translate';
import { Language } from './../database/entities/language.entity';
import { TranslationDTO } from './models/translation.dto';

@Injectable()
export class TranslationsService {

    private readonly translate = new GoogleTranslate.v2.Translate();

    public constructor(
        @InjectRepository(Rating) private readonly ratingRepository: Repository<Rating>,
        @InjectRepository(Translation) private readonly translationsRepository: Repository<Translation>,
        @Inject(forwardRef(() => LanguagesService)) private readonly languageService: LanguagesService,
    ) { }

    public async getAllTranslations(criteria: GetTranslationDTO, skip?: number, take?: number): Promise<Translation[]> {
        let translations: Translation[];
        try {
            translations = await this.translationsRepository.find({
                where: criteria,
                relations: ['rating'],
                skip,
                take,
            });
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        return translations;
    }

    public async getAllTranslationsByContent(content: string): Promise<Translation[] | Translation> {
        let translations: Translation[];
        try {
            translations = await this.translationsRepository.find({
                where: [{
                    originalText: Like(`%${content}%`),
                }, {
                    translation: Like(`%${content}%`),
                }],
                take: 30,
            });
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        return translations;
    }

    public async getTranslationForArticle(criteria: GetTranslationDTO): Promise<ArticleTranslationDTO> {
        const translation: Translation = await this.getTranslation(undefined, criteria);
        const articleTranslation: ArticleTranslationDTO = {
            originalText: criteria.originalText,
            translatedText: translation.translation,
            rating: {
                id: translation.rating.id,
                avgrating: translation.rating.avgrating,
            },
        };
        return articleTranslation;
    }

    public async getTranslation(id: string, criteria: GetTranslationDTO): Promise<Translation> {
        let translation: Translation;

        if (id) {
            criteria = { ...criteria, id };
        }
        try {
            translation = await this.translationsRepository.findOne({
                where: criteria,
                relations: ['rating'],
            });
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        if (translation === undefined) {
            throw new MultiLingoSystemError(`No such translation!`, HttpStatus.BAD_REQUEST);
        }

        return translation;
    }

    public async createTranslation(body: CreateTranslationDTO): Promise<Translation[]> {
        const allLanguages: Language[] = await this.languageService.getAllLanguage();
        const criteriaForTranslation: CreateTranslationDTO[] = allLanguages
            .filter((language: Language) => language.isActive && (language.code !== body.originalLanguageCode))
            .map((language: Language) => ({ ...body, targetLanguageCode: language.code }));
        const promiseTranslations = criteriaForTranslation
            .map((criteria: CreateTranslationDTO) => this.translateText(criteria));
        let translations: string[];
        try {
            translations = await Promise.all(promiseTranslations);
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }

        const ratings: Rating[] = translations.map(() => this.ratingRepository.create());
        const promiseRatings: Array<Promise<Rating>> = ratings.map((rating: Rating) => this.ratingRepository.save(rating));
        const savedRatings: Rating[] = await Promise.all(promiseRatings);

        const translationEntities: Array<Promise<Translation>> = translations
            .map((translation, i) => {
                const createTranslation: TranslationDTO = {
                    originalText: criteriaForTranslation[i].originalText,
                    originalLanguageCode: criteriaForTranslation[i].originalLanguageCode,
                    translation,
                    targetLanguageCode: criteriaForTranslation[i].targetLanguageCode,
                    rating: savedRatings[i],
                };
                const createdTranslation = this.translationsRepository.create(createTranslation);
                return this.translationsRepository.save(createdTranslation);
            });
        let savedTranslations: Translation[];
        try {
            savedTranslations = await Promise.all(translationEntities);
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        return savedTranslations;
    }

    public async updateTranslation(id: string, translation: string): Promise<Translation> {
        const foundTranslation: Translation = await this.getTranslation(id, undefined);
        if (foundTranslation === undefined) {
            throw new MultiLingoSystemError(
                `No such translation!`,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        const updatedTranslation = { ...foundTranslation, translation };
        const rating: Rating = foundTranslation.rating;
        rating.avgrating = 0;
        rating.rating = 0;
        rating.voted = 0;

        let savedRating: Rating;
        try {
            savedRating = await this.ratingRepository.save(rating);
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        updatedTranslation.rating = savedRating;
        let savedTranslation: Translation;
        try {
            savedTranslation = await this.translationsRepository.save(updatedTranslation);
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        return savedTranslation;
    }

    public async changeTranslationDeleteStatus(id: string): Promise<Translation> {
        const foundTranslation: Translation = await this.getTranslation(id, undefined);
        if (foundTranslation === undefined) {
            throw new MultiLingoSystemError(`No such translation!`, HttpStatus.BAD_REQUEST);
        }
        foundTranslation.isDeleted = !foundTranslation.isDeleted;
        let savedTranslation: Translation;
        try {
            savedTranslation = await this.translationsRepository.save(foundTranslation);
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        return savedTranslation;
    }

    public async rateTranslation(id: string, rating: number): Promise<Rating> {
        let foundRating: Rating;
        try {
            foundRating = await this.ratingRepository.findOne({ id });
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }
        if (foundRating === undefined) {
                throw new MultiLingoSystemError(
                    `No such translation rating!`,
                    HttpStatus.BAD_REQUEST,
                );
            }
        foundRating.voted++;
        foundRating.rating += rating;
        foundRating.avgrating = foundRating.rating / foundRating.voted;

        let savedRating: Rating;
        try {
            savedRating = await this.ratingRepository.save(foundRating);
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }

        return savedRating;
    }

    public async translateText(criteria: CreateTranslationDTO) {
        let translation;
        try {
            [translation] = await this.translate.translate(criteria.originalText, criteria.targetLanguageCode);
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }

        return translation;
    }

}
