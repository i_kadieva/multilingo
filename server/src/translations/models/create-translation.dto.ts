import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateTranslationDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public originalText: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public originalLanguageCode: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public targetLanguageCode: string;

}
