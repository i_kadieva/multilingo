import { ShowRatingDTO } from './../../rating/models/show-rating.dto';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ArticleTranslationDTO {

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public originalText: string;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public translatedText: string;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    public rating: ShowRatingDTO;

}
