import { ShowRatingDTO } from './../../rating/models/show-rating.dto';
import { Rating } from './../../database/entities/rating.entity';
import { Publish } from './../../common/middleware/transformer/decorators/publish';
import { ApiModelProperty } from '@nestjs/swagger';

export class ShowTranslationDTO {

    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish()
    public originalText: string;

    @ApiModelProperty()
    @Publish()
    public translation: string;

    @ApiModelProperty()
    @Publish()
    public isDeleted?: boolean;

    @ApiModelProperty()
    @Publish()
    public originalLanguageCode: string;

    @ApiModelProperty()
    @Publish()
    public targetLanguageCode: string;

    @ApiModelProperty()
    @Publish(ShowRatingDTO)
    public rating: Rating;

}
