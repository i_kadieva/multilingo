import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsBoolean } from 'class-validator';

export class GetTranslationDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public id?: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public originalText?: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public originalLanguageCode?: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public targetLanguageCode?: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsBoolean()
    public isDeleted?: boolean;

}
