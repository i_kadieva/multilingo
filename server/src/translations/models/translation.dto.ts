import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsBoolean } from 'class-validator';
import { Rating } from './../../database/entities/rating.entity';

export class TranslationDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public originalText: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public originalLanguageCode: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public targetLanguageCode: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsBoolean()
    public translation: string;

    @ApiModelProperty()
    @IsNotEmpty()
    public rating: Rating;

}
