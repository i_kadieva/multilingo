import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateTranslationDTO {

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public translation: string;

}
