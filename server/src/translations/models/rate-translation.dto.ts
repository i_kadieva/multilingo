import { ApiModelProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, Max, Min } from 'class-validator';

export class RateTranslationDto {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsInt()
    @Min(0)
    @Max(5)
    public rating: number;
}
