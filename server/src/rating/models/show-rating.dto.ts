import { Publish } from './../../common/middleware/transformer/decorators/publish';
import { ApiModelProperty } from '@nestjs/swagger';

export class ShowRatingDTO {

    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish()
    public avgrating: number;

}
