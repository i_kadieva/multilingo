export enum RoleType {
    Contributor = 'Contributor',
    Editor = 'Editor',
    Administrator = 'Administrator',
}
