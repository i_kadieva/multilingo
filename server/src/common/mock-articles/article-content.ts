import {CreateArticleDTO} from '../../articles/models/create-article.dto';

export const articleArrayForAdmin: CreateArticleDTO[] = [
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/4IEjMOocrQ4cjj2yS5zB0j/8f24a51220ca15e0df1948084715c876/Two_Popes__Large.png',
        title: 'The two popes',
        content: 'On this rock I will build my church, and the gates of hell shall not prevail against it.So Jesus told Peter in Matthew 16:18. And so the Catholic Church has taken as its founding verse. Peter is recognized as the first pope, and he allegedly died on Vatican Hill, where the church named for him now stands. His tomb is located under its floor.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/6coKWUjFAvP2NaBlICgoZc/658d6011ce899c68f4a726d64321c1f5/Dark_Waters__Large.jpg',
        title: 'Dark-waters',
        content: 'Rob Bilott was never a guy who made much of an impression. He wasn’t flashy or very well dressed. He schlepped around in one of several older model cars that he liked to collect. In fact, he was something of a perfect choice for a corporate defense lawyer: a young family guy who blended into the pack, knew the minutia of law inside and out, and worked hard to keep everything on an even keel.And being even-keeled, in the corporate world, is very, very good.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/5jqLKHIcuoriaER2y7w5vW/5951a0fb5e11e9464398ba56bec3c6c7/Pokemon_Shield__Large.jpg',
        title: 'Pokemon sword andshield',
        content: 'After 23 years of cranking out generation after generation of Pokémon video games, you’d think that there couldn’t possibly be anything left to add to one of these little creature-catching romps. I mean, they are what they are, so just keep following that winning formula, right?',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/4FiQvnHSJ0rPGyzs5fnG41/e842cb314796e95f7451136c1fe352a9/Skillet__Victorious__Large.jpg',
        title: 'Skillet Victorious',
        content: 'The Wisconsin-bred act has sold 12 million albums and hit the two-billion stream mark on Spotify. It\'s enjoyed 21 chart-topping songs on the Christian singles chart. And husband-and-wife teammates John and Korey Cooper, along with drummer Jen Ledger and guitarist Seth Morrison, continue to pound out ferociously hard-rocking albums that revolve around two ideas: Life is hard. But there is hope.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/2X095A7plMQXPAxAMOvxD2/989676a4b84c491914dbbeaf29defe29/rabbit-hill-cover.jpg',
        title: 'Rabbit hill',
        content: 'The animals on the Hill continue to speculate what new food the people might plant there. Moving vans arrive, and the animals believe the furniture indicates the new owners are quality people. A man, two women and a cat later arrive by car. The animals’ minds are eased when they see the cat is old and not anxious to chase anyone.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/oYjHY5U5XiqG8aooswI6g/8c61338369c529233637e95727efbbb5/Mr._Robot_Large.jpg',
        title: 'Mr.Robot',
        content: 'Elliot Alderson has a problem. Several, actually.He has an anxiety disorder. He doesn\'t like to be touched. He takes morphine. He hallucinates. Oh, and he thinks an omnipotent corporate entity is out to get him. Elliot\'s biggest problem? He may be right about that last one. And the omnipotent corporate enemy is larger and darker than he ever imagined.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/68OenpYEjmasQOKeGc6s8A/8ff5ad782d566b3ed8ad977a66c1cb2d/Rick_and_Morty__Large.jpg.jpeg',
        title: 'Rick and Morty',
        content: 'Take Rick Sanchez, for instance. After having been gone—like, really gone—for a couple of decades, the old man with the blue pointy hair suddenly shows up on daughter Beth\'s doorstep and moves in. It\'s obvious to everyone that he\'s not exactly, um, right, if you know what I mean. But perhaps that\'s simply a side effect of his adventures—courtesy of a portal-creating gun— through an unfolding and chaotic multiverse.',
        languageCode: 'en',
    },
];
export const articleArrayForUser: CreateArticleDTO[] = [
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/6qgOAjw1VrB9y8Cnq8Fm2Q/37969c3ce079eea785fe869560901989/Islands__Large.jpg',
        title: 'The islands',
        content: 'Instead of being warmly welcomed, the missionaries were suspiciously regarded by the native peoples, who were accustomed to the destructive, violent ways of colonization and trade. But there was one native who cautiously opened her village to the missionaries: Chiefess Kapi’olani.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/7e3eKjsjTafEAxTAEFDA2D/3c1cb107f657df70a0f944f536368151/Daybreak_Large.jpg',
        title: 'Daybreak',
        content: 'Only, it’s been a few months since everything fell apart and he still doesn’t know where she is. Here’s what he does know: The bomb that hit their city was biological. It instantly vaporized half the population, and the adults who survived were turned into “ghoulies.” With their mindless wandering and hankering for flesh, they certainly seem like zombies, but their bites won’t kill you or turn you. (Unfortunately Josh doesn’t find this out until after he chops off his finger in a poor attempt to save himself from a bite.)',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/6LDwymGPCQvVcOiEMJeL4E/0bc91b54a2db5bc1a377090e1f4d643a/Logic__Large.jpg',
        title: 'Logic confessions of a dangerous mind',
        content: 'Logic was raised by drug-addicted parents and surrounded by siblings who sold drugs while he grew up Gaithersburg, Maryland. After being expelled from high school, Logic turned to music as a way to escape and to immerse himself in what he loved most.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/ygS47LOLtxehj2mD1QyYK/694d6c8bcfd1c671d942599543cb8c0d/Imperator_Rome__Large.jpg',
        title: 'Imperator Rome',
        content: 'The main goal of Imperator is to gaze at all the many clans, tribes and nations bunched around your budding Roman Republic, and figure out how to make all these political entities on the game\'s massive historical map your own. But it’s not as simple as just unleashing the dogs of war. There’s a Senate you’ll need to manage, a national budget to maintain, populations to placate, armies to raise. And it’s never an easy go.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/5E4k49Z74giCb89UtRgEO5/060cc39706a340eec6450d5100786465/echo-cover.jpg',
        title: 'Echo',
        content: 'During a game of hide-and-seek, Otto pauses to read a storybook about three lost princesses named Eins, Zwei and Drei, who are cast out by their father and raised by a witch in the forest. Otto gets lost in the woods. He trips and falls, knocking himself unconscious. When he wakes up, he finds himself with Eins, Zwei and Drei, who ask him to read them their own story.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/1I9g9BN42kEJ5nt8FFXAol/ef81b4546f18dd31d2ead32a04583814/Joker__Large__.jpg',
        title: 'Joker',
        content: 'The question comes freighted with a certain level of irony. After all, Arthur is talking with his legally mandated counselor after a stint in an asylum. He takes seven kinds of psychiatric meds—none of which seem to help one of his most obvious conditions, in which he laughs wildly at the most inopportune times.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/2KDPRzRqYZfDyRHLKuuVFf/bbfa0d660f313ebcdf48d112b11ec546/end_of_the_world_Large.jpg',
        title: 'The end of the fucking world',
        content: 'Most teens probably feel a little lost and alone at some point. Weird and unloved. They look in the mirror, stare deeply at the face staring back at them and think to themselves, sincerely, "Boy, am I messed up!" (Or some variation thereof).',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/6wxA0iItPmTq84HVUWq8PV/de768f301294c35f9093a55188bf1294/Jesus_is_King_Large.jpg',
        title: 'Jesus is king',
        content: 'Kanye West has talked about Jesus since the beginning of his career. “Jesus Walks” put him on the rap map back in 2004, and nods to Jesus have permeated his work from time to time since.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/2eYjzCena2GTkwIEOJbhYK/d33699261d856a28354075ea71aeb334/Control__Large.jpg',
        title: 'Control',
        content: 'Some developers lean toward the gunplay of historical wars. Others might focus on a dystopian anarchy, an alien infestation or a zombie invasion.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/6yguSOpzcIYWX3SU7JoWGT/ff83fa42c3aab57ac3894b708fda2c14/dr-birds-advice-for-sad-poets-cover.jpg',
        title: 'Dr Birds advice for sad poets',
        content: 'James is shocked and thrilled when Beth approaches him at school. Before she was expelled, his older sister, Jorie, wrote for the school’s literary paper. As one of the editors, Beth asks James to find an illustrated story that Jorie was working on before she left. James visits Jorie’s old room but finds nothing.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/4ItsRPyNVTLqLPYPMqc7Qa/0112723f89d54a4546c878866dd38038/Ford_v_Ferrari__Large.jpg',
        title: 'Ford vs Ferrari',
        content: 'Once upon a time, Henry Ford II decided to buy up a little Italian car company called Ferrari. You know, to add a bit of racing cred to his family-car image. I mean, Ferrari’s meticulously hand-crafted sports cars offered a lot of panache, but selling them at their required price point, even in the early 1960s, wasn’t easy. Ferrari was losing money hand over fist. Ford at that time, however, had plenty of cash. So it seemed to be a match made in motoring heaven.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/6oAmCe1j8I0q0206mc6OGO/5b96e31390641132ca42df2b5f906126/archer-review-image.jpg',
        title: 'Archer',
        content: 'Now imagine that 007 has been captured by Ernst Blofeld (that evil, cat-stroking mastermind of Specter) and has been chained to a gurney, with a massive, Plasticine ray gun pointed at his forehead.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/ejdolzw1UZqaEtcINKeaU/5622c6cb9fe580ef45a1a844128b8e15/Billie_Eilish__don_t_smile_at_me__Large.jpg',
        title: 'Dont smile at me',
        content: 'Becoming wildly famous after releasing a song on SoundCloud might be the dream of many starry-eyed teens. But that wasn\'t what Los Angeles native Billie Eilish anticipated. At the age of 14, she uploaded her song “Ocean Eyes” onto that platform as a homework assignment and didn’t think much of it. Until, that is, it blew up and garnered milllions of streams. Not long after, the teen began to be hounded for more.',
        languageCode: 'en',
    },
    {
        imgPath: 'https://images.ctfassets.net/7h71s48744nc/3PY2GXod322quwgswS8y4y/122b919376086ce6c76af7820c5f5f4e/Marvel_s_Spider-Man__Large.jpg',
        title: 'Spider-man',
        content: 'In this tangled-web story, Peter Parker—the hero whom we all know gained wall-climbing superpowers due to a radioactive spider-bite—is now out of college and making his way in the big city of Manhattan. While, incidentally, trying to make rent on a tiny, hole-in-the-wall apartment.',
        languageCode: 'en',
    },
];
