import {CreateUielementDto} from '../../ui-component/models/create-uielement.dto';

export const UIElements: CreateUielementDto[] = [
    {
        content: 'Language name',
        resourceName: 'language_name',
        languageCode: 'en',
    },
    {
        content: 'Language code',
        resourceName: 'language_code',
        languageCode: 'en',
    },
    {
        content: 'deleted',
        resourceName: 'deleted',
        languageCode: 'en',
    },
    {
        content: 'Load more',
        resourceName: 'load_more',
        languageCode: 'en',
    },
    {
        content: 'Articles',
        resourceName: 'articles',
        languageCode: 'en',
    },
    {
        content: 'French',
        resourceName: 'placeholder',
        languageCode: 'en',
    },
    {
        content: 'Input invalid',
        resourceName: 'invalid_input',
        languageCode: 'en',
    },
    {
        content: 'Valid invalid',
        resourceName: 'valid_input',
        languageCode: 'en',
    },
    {
        content: 'Add language',
        resourceName: 'add_language',
        languageCode: 'en',
    },
    {
        content: 'Name',
        resourceName: 'name',
        languageCode: 'en',
    },
    {
        content: 'Code',
        resourceName: 'code',
        languageCode: 'en',
    },
    {
        content: 'Active',
        resourceName: 'active',
        languageCode: 'en',
    },
    {
        content: 'Roles',
        resourceName: 'roles',
        languageCode: 'en',
    },
    {
        content: 'Info',
        resourceName: 'info',
        languageCode: 'en',
    },
    {
        content: 'Editor',
        resourceName: 'editor',
        languageCode: 'en',
    },
    {
        content: 'Admin',
        resourceName: 'admin',
        languageCode: 'en',
    },
    {
        content: 'Contributor',
        resourceName: 'contributor',
        languageCode: 'en',
    },
    {
        content: 'Languages',
        resourceName: 'languages',
        languageCode: 'en',
    },
    {
        content: 'Users',
        resourceName: 'users',
        languageCode: 'en',
    },
    {
        content: 'MultiLingo articles portal',
        resourceName: 'MAP',
        languageCode: 'en',
    },
    {
        content: 'Login',
        resourceName: 'login',
        languageCode: 'en',
    },
    {
        content: 'Register',
        resourceName: 'register',
        languageCode: 'en',
    },
    {
        content: 'Sign in',
        resourceName: 'sign_in',
        languageCode: 'en',
    },
    {
        content: 'Username or e-mail',
        resourceName: 'UOE',
        languageCode: 'en',
    },
    {
        content: 'The username or email is required',
        resourceName: 'tuoeir',
        languageCode: 'en',
    },
    {
        content: 'The username or email must be at least 2 symbols long',
        resourceName: 'err1',
        languageCode: 'en',
    },
    {
        content: 'The username or email must be shorter than 20 symbols',
        resourceName: 'err2',
        languageCode: 'en',
    },
    {
        content: 'Input required',
        resourceName: 'input_required',
        languageCode: 'en',
    },
    {
        content: 'The password must be minimum eight characters, one letter and one number',
        resourceName: 'ps-msg',
        languageCode: 'en',
    },
    {
        content: 'Password',
        resourceName: 'ps',
        languageCode: 'en',
    },
    {
        content: 'My articles',
        resourceName: 'my_articles',
        languageCode: 'en',
    },
    {
        content: 'Search',
        resourceName: 'search',
        languageCode: 'en',
    },
    {
        content: 'Logout',
        resourceName: 'logout',
        languageCode: 'en',
    },
    {
        content: 'Translations',
        resourceName: 'trl',
        languageCode: 'en',
    },
    {
        content: 'Admin panel',
        resourceName: 'AP',
        languageCode: 'en',
    },
    {
        content: 'Username',
        resourceName: 'us',
        languageCode: 'en',
    },
    {
        content: 'E-mail',
        resourceName: 'em',
        languageCode: 'en',
    },
    {
        content: 'Avatar path',
        resourceName: 'avatar_path',
        languageCode: 'en',
    },
    {
        content: 'Wrong email format',
        resourceName: 'email_format',
        languageCode: 'en',
    },
    {
        content: 'Choose language',
        resourceName: 'c_language',
        languageCode: 'en',
    },
    {
        content: 'Sorry, it\'s me, not you',
        resourceName: 'simny',
        languageCode: 'en',
    },
    {
        content: 'Let me try again',
        resourceName: 'lmta',
        languageCode: 'en',
    },
    {
        content: 'ID',
        resourceName: 'id',
        languageCode: 'en',
    },
    {
        content: 'Original Text',
        resourceName: 'OD',
        languageCode: 'en',
    },
    {
        content: 'Original language code',
        resourceName: 'OLC',
        languageCode: 'en',
    },
    {
        content: 'Target language code',
        resourceName: 'TLC',
        languageCode: 'en',
    },
    {
        content: 'Rating',
        resourceName: 'rating',
        languageCode: 'en',
    },
    {
        content: 'Actions',
        resourceName: 'action',
        languageCode: 'en',
    },
    {
        content: 'Edit',
        resourceName: 'edit',
        languageCode: 'en',
    },
    {
        content: 'Close',
        resourceName: 'close',
        languageCode: 'en',
    },
    {
        content: 'Save',
        resourceName: 'save',
        languageCode: 'en',
    },
    {
        content: 'Used technologies',
        resourceName: 'used_technologies',
        languageCode: 'en',
    },
    {
        content: 'Copyright: Team',
        resourceName: 'CT',
        languageCode: 'en',
    },
    {
        content: 'Create article',
        resourceName: 'create_article',
        languageCode: 'en',
    },
    {
        content: 'Create your article here',
        resourceName: 'cyah',
        languageCode: 'en',
    },
    {
        content: 'Enter image URL...',
        resourceName: 'eiURL',
        languageCode: 'en',
    },
    {
        content: 'The title is required',
        resourceName: 'ttir',
        languageCode: 'en',
    },
    {
        content: 'Enter title...',
        resourceName: 'enter_title',
        languageCode: 'en',
    },
    {
        content: 'Article\'s content here...',
        resourceName: 'article_content',
        languageCode: 'en',
    },
    {
        content: 'Create',
        resourceName: 'create',
        languageCode: 'en',
    },
    {
        content: 'Confirmation',
        resourceName: 'conf',
        languageCode: 'en',
    },
    {
        content: 'Cancel',
        resourceName: 'cancel',
        languageCode: 'en',
    },
    {
        content: 'Ok',
        resourceName: 'ok',
        languageCode: 'en',
    },
    {
        content: 'Versions',
        resourceName: 'version',
        languageCode: 'en',
    },
    {
        content: 'Edit article',
        resourceName: 'edit_article',
        languageCode: 'en',
    },
    {
        content: 'Edit your article here',
        resourceName: 'eyah',
        languageCode: 'en',
    },
    {
        content: 'Picture',
        resourceName: 'picture',
        languageCode: 'en',
    },
    {
        content: 'Title',
        resourceName: 'title',
        languageCode: 'en',
    },
    {
        content: 'Content',
        resourceName: 'content',
        languageCode: 'en',
    },
    {
        content: 'Current',
        resourceName: 'current',
        languageCode: 'en',
    },
    {
        content: 'View',
        resourceName: 'view',
        languageCode: 'en',
    },
    {
        content: 'User deleted!',
        resourceName: 'user_deleted',
        languageCode: 'en',
    },
    {
        content: 'New language added!',
        resourceName: 'user_added',
        languageCode: 'en',
    },
    {
        content: 'Role changed!',
        resourceName: 'role-change',
        languageCode: 'en',
    },
    {
        content: 'New published!',
        resourceName: 'new_published',
        languageCode: 'en',
    },
    {
        content: 'More articles!',
        resourceName: 'more_articles',
        languageCode: 'en',
    },
    {
        content: 'Article created!',
        resourceName: 'article_created',
        languageCode: 'en',
    },
    {
        content: 'Article deleted!',
        resourceName: 'article_deleted',
        languageCode: 'en',
    },
    {
        content: 'Article deleted!',
        resourceName: 'article_deleted',
        languageCode: 'en',
    },
    {
        content: 'Rate title translation',
        resourceName: 'rate_translation',
        languageCode: 'en',
    },
    {
        content: 'Rate article translation',
        resourceName: 'rate_article',
        languageCode: 'en',
    },
    {
        content: 'The image URL is required!',
        resourceName: 'url_required',
        languageCode: 'en',
    },
    {
        content: 'Article updated!',
        resourceName: 'article_updated',
        languageCode: 'en',
    },
    {
        content: 'Successful sign in!',
        resourceName: 'toaster_sign_in',
        languageCode: 'en',
    },
    {
        content: 'Successful sign out!',
        resourceName: 'toaster_sign_out',
        languageCode: 'en',
    },
    {
        content: 'Sign out failed!',
        resourceName: 'sign_err',
        languageCode: 'en',
    },
    {
        content: 'Successful registration!',
        resourceName: 'toaster_reg',
        languageCode: 'en',
    },
    {
        content: 'Ops something went wrong',
        resourceName: 'ops',
        languageCode: 'en',
    },
    {
        content: 'Translation edited!',
        resourceName: 'translation_edited',
        languageCode: 'en',
    },
    {
        content: 'Translation rated',
        resourceName: 'translation_rated',
        languageCode: 'en',
    },
    {
        content: 'You do not have any articles yet!',
        resourceName: 'no_articles',
        languageCode: 'en',
    },
    {
        content: 'Unauthorized access!',
        resourceName: 'un_access',
        languageCode: 'en',
    },
    {
        content: 'Set as current',
        resourceName: 'sac',
        languageCode: 'en',
    },
];
