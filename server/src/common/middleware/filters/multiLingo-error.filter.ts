import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Response } from 'express';
import { MultiLingoSystemError } from '..';

@Catch(MultiLingoSystemError)
export class MultiLingoErrorFilter implements ExceptionFilter {
  public catch(exception: MultiLingoSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
