import { SetMetadata } from '@nestjs/common';
import { RoleType} from '../../enums/role-type';

// tslint:disable-next-line: variable-name
export const Roles = (...roles: RoleType[]) => SetMetadata('roles', roles);
