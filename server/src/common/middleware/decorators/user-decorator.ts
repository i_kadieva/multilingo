import {createParamDecorator} from '@nestjs/common';

export const USER = createParamDecorator((_, req) => req.user);
