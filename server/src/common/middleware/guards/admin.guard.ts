import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { RoleType} from '../../enums/role-type';
import { Role} from '../../../database/entities/role.entity';
import { User} from '../../../database/entities/user.entity';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  public canActivate(context: ExecutionContext): boolean {
    const roles: RoleType[] = this.reflector.get<RoleType[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user: User = request.user;
    const hasRole = () => user.roles.some((role: Role) => roles.includes(role.name));
    if (user && user.roles && hasRole()) {
      return true;
    }
  }
}
