import {
  Injectable,
  CanActivate,
  ExecutionContext,
  HttpStatus,
} from '@nestjs/common';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { MultiLingoSystemError } from './../exceptions/multiLingo-system.error';
import { AuthService } from '../../../auth/auth.service';

@Injectable()
export class AuthGuardWithBlacklisting extends AuthGuard('jwt')
  implements CanActivate {
  public constructor(private readonly authService: AuthService) {
    super();
  }
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    if (!(await super.canActivate(context))) {
      return false;
    }

    const request: Request = context.switchToHttp().getRequest();
    const token = request.headers.authorization;

    let isBlacklisted: boolean;
    try {
      isBlacklisted = await this.authService.isTokenBlacklisted(token);
    } catch (error) {
      throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
    }
    if (isBlacklisted) {
      throw new MultiLingoSystemError('Unauthorized access!', HttpStatus.UNAUTHORIZED);
    }

    return true;
  }
}
