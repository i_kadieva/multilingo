import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ConfigService } from './config/config.service';
import * as rateLimit from 'express-rate-limit';
import * as helmet from 'helmet';
import { MultiLingoErrorFilter } from './common/middleware';
import { ArticlesModule } from './articles/articles.module';
import { UiComponentModule } from './ui-component/ui-component.module';
import { TranslationsModule } from './translations/translations.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));
  app.useGlobalFilters(new MultiLingoErrorFilter());

  const options = new DocumentBuilder()
    .setTitle('MultiLingo - Translation App / Users')
    .setDescription(`The MultiLingo - Translation App
      is an application for sharing and translating articles regarding user\'s locale language`)
    .setVersion('1.0')
    .addTag('users')
    .addBearerAuth()
    .build();

  const usersDocument = SwaggerModule.createDocument(app, options, {
    include: [UsersModule],
  });
  SwaggerModule.setup('multilingo/swagger/users', app, usersDocument);

  const secondOptions = new DocumentBuilder()
  .setTitle('MultiLingo - Translation App / Articles')
  .setDescription(`The MultiLingo - Translation App
     is an application for sharing and translating articles regarding user\'s locale language`)
  .setVersion('1.0')
  .addTag('articles')
  .addBearerAuth()
  .build();

  const articlesDocument = SwaggerModule.createDocument(app, secondOptions, {
    include: [ArticlesModule],
  });
  SwaggerModule.setup('multilingo/swagger/articles', app, articlesDocument);

  const seconddOptions = new DocumentBuilder()
  .setTitle('MultiLingo - Translation App / UIElements')
  .setDescription(`The MultiLingo - Translation App
  is an application for sharing and translating articles regarding user\'s locale language`)
  .setVersion('1.0')
  .addTag('UIElements')
  .addBearerAuth()
  .build();

  const uiElementsDocument = SwaggerModule.createDocument(app, seconddOptions, {
    include: [UiComponentModule],
  });
  SwaggerModule.setup('multilingo/swagger/uiElements', app, uiElementsDocument);

  const thirdOptions = new DocumentBuilder()
  .setTitle('MultiLingo - Translation App / Elements')
  .setDescription(`The MultiLingo - Translation App
     is an application for sharing and translating articles regarding user\'s locale language`)
  .setVersion('1.0')
  .addTag('session')
  .addBearerAuth()
  .build();

  const loginDocument = SwaggerModule.createDocument(app, thirdOptions, {
    include: [AuthModule],
  });

  SwaggerModule.setup('multilingo/swagger/session', app, loginDocument);

  const fourthOptions = new DocumentBuilder()
      .setTitle('MultiLingo - Translation App / UIElements')
      .setDescription(`The MultiLingo - Translation App
  is an application for sharing and translating articles regarding user\'s locale language`)
      .setVersion('1.0')
      .addTag('UIElements')
      .addBearerAuth()
      .build();

  const translationsDocument = SwaggerModule.createDocument(app, fourthOptions, {
    include: [TranslationsModule],
  });
  SwaggerModule.setup('multilingo/swagger/translations', app, uiElementsDocument);

  app.use(helmet());
  app.use(
      rateLimit({
        windowMs: 15 * 60 * 1000, // 15 minutes
        max: 1000, // Limit each IP to 100 requests per windowMs
      }),
  );
  app.enableShutdownHooks();
  await app.listen(app.get(ConfigService).port);
}
bootstrap();
