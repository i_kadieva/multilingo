import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Post,
    Put,
    UseGuards, UseInterceptors,
    ValidationPipe,
} from '@nestjs/common';
import { Language } from './../database/entities/language.entity';
import { LanguagesService} from './languages.service';
import { RoleType} from '../common/enums/role-type';
import { RoleGuard, Roles} from '../common/middleware';
import { AuthGuard} from '@nestjs/passport';
import { ShowLanguageDTO} from './models/show-languages.dto';
import { CreateLanguagesDto} from './models/create-languages.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiOkResponse, ApiUseTags} from '@nestjs/swagger';
import { UpdateLanguagesDto} from './models/update-languages.dto';
import { TransformInterceptor } from '../common/middleware/transformer/interceptors/transform.interceptor';
import { AuthGuardWithBlacklisting } from '../common/middleware/guards/blacklist.guard';

@ApiUseTags('languages')
@ApiBearerAuth()
@Controller('multilingo/languages')
export class LanguagesController {
    public constructor(
        private readonly languagesService: LanguagesService,
    ) {}

    @Get()
    @UseInterceptors(new TransformInterceptor(ShowLanguageDTO))
    @ApiOkResponse({description: 'Returns all languages!', type: ShowLanguageDTO})
    @ApiBadRequestResponse({description: 'No languages!'})
    @HttpCode(HttpStatus.OK)
    public async getAllLanguages(): Promise<Language[]> {
        return await this.languagesService.getAllLanguage();
    }

    @Post()
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowLanguageDTO))
    @ApiOkResponse({description: 'Returns all languages + the new one!', type: ShowLanguageDTO})
    @ApiBadRequestResponse({description: 'Something went wrong. Please try again later...'})
    @HttpCode(HttpStatus.CREATED)
    public async addLanguage(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    })) body: CreateLanguagesDto): Promise<Language[]> {
        return await this.languagesService.createLanguage(body);
    }

    @Delete('/:id')
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @ApiOkResponse({description: 'Returns deleted language!', type: ShowLanguageDTO})
    @ApiBadRequestResponse({description: 'Language with such id does not exists!'})
    @HttpCode(HttpStatus.OK)
    public async deleteLanguage(@Param('id') languageId: string): Promise<Language> {
        return await this.languagesService.deactivateLanguage(languageId);
    }

    @Put('/:id')
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowLanguageDTO))
    @ApiOkResponse({description: 'Returns updated language!', type: ShowLanguageDTO})
    @ApiBadRequestResponse({description: 'Language with such id does not exists!'})
    @HttpCode(HttpStatus.OK)
    public async updateLanguage(
        @Param('id') languageId: string,
        @Body(new ValidationPipe({transform: true, whitelist: true})) body: Partial<UpdateLanguagesDto>,
    ): Promise<ShowLanguageDTO> {
        return await this.languagesService.updateLanguage(languageId, body);
    }

}
