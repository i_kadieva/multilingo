import { Rating } from './../database/entities/rating.entity';
import { TranslationsService } from './../translations/translations.service';
import { Translation } from './../database/entities/translation.entity';
import { Language } from '../database/entities/language.entity';
import { forwardRef, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MultiLingoSystemError } from '../common/middleware';
import { CreateLanguagesDto } from './models/create-languages.dto';
import { UpdateLanguagesDto } from './models/update-languages.dto';
import { Repository } from 'typeorm';

@Injectable()
export class LanguagesService {

    public constructor(
        @InjectRepository(Language) private readonly languageRepository: Repository<Language>,
        @InjectRepository(Rating) private readonly ratingRepository: Repository<Rating>,
        @InjectRepository(Translation) private readonly translationsRepository: Repository<Translation>,
        @Inject(forwardRef(() => TranslationsService)) private readonly translationsService: TranslationsService,
    ) {
    }

    public async getAllLanguage(): Promise<Language[]> {
        let languages: Language[];
        try {
            languages = await this.languageRepository.find();
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return languages;
    }

    public async createLanguage(body: CreateLanguagesDto): Promise<Language[]> {
        const foundLanguageName: Language = await this.languageRepository.findOne({ name: body.name });
        if (foundLanguageName) {
            throw new MultiLingoSystemError(`Language with such name already exists!`, HttpStatus.BAD_REQUEST);
        }
        const foundLanguageCode: Language = await this.languageRepository.findOne({ code: body.code });
        if (foundLanguageCode) {
            throw new MultiLingoSystemError(`Language with such code already exists!`, HttpStatus.BAD_REQUEST);
        }
        let language: Language;
        try {
            const languageEntity: Language = this.languageRepository.create(body);
            language = await this.languageRepository.save(languageEntity);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.BAD_REQUEST);
        }
        await this.createNewLanguageTranslations(language.code);

        return await this.languageRepository.find({});
    }

    public async updateLanguage(languageId: string, body: Partial<UpdateLanguagesDto>): Promise<Language> {
        const foundLanguage = await this.languageRepository.findOne({ id: languageId });
        if (foundLanguage === undefined) {
            throw new MultiLingoSystemError(`Language with such id does not exists!`, HttpStatus.BAD_REQUEST);
        }

        const upLanguage = { ...foundLanguage, body };
        let language: Language;
        try {
            language = await this.languageRepository.save(upLanguage);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }

        return language;
    }

    public async deactivateLanguage(languageId: string): Promise<Language> {
        const foundLanguage = await this.languageRepository.findOne({ id: languageId });
        if (foundLanguage === undefined) {
            throw new MultiLingoSystemError(`Language with such id does not exists!`, HttpStatus.BAD_REQUEST);
        }
        let language: Language;
        try {
            language = await this.languageRepository.save({ ...foundLanguage, isActive: false });
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }

        return language;
    }

    public async getLanguageName(code: string, withDeleted: boolean): Promise<string> {
        let language: Language;
        try {
            if (withDeleted) {
                const where = { name, isDeleted: withDeleted };
                language = await this.languageRepository.findOne({ where });
            } else {
                language = await this.languageRepository.findOne({ code });
            }
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        return language.name;
    }

    private async createNewLanguageTranslations(translateTo: string) {
        const translations: Translation[] =
            await this.translationsService.getAllTranslations({});
        const translatedOrigin: string[] = [];
        const newTranslations: Translation[] = await translations.reduce(async (acc, translation: Translation) => {
            const accumulator = await Promise.resolve(acc);
            const {id, isDeleted, rating,  ...temp } = translation;
            if (!translatedOrigin.includes(translation.originalText)) {
                const criteria = {
                    originalText: translation.originalText,
                    originalLanguageCode: translation.originalLanguageCode,
                    targetLanguageCode: translateTo,
                };
                temp.translation = (await this.translationsService.translateText(criteria));
                temp.targetLanguageCode = translateTo;
                translatedOrigin.push(temp.originalText);
                accumulator.push(temp);
                acc = Promise.resolve(accumulator);
            }
            return acc;
        }, Promise.resolve([]));

        const ratings: Rating[] = newTranslations.map(() => this.ratingRepository.create());
        const promiseRatings: Array<Promise<Rating>> = ratings.map((rating: Rating) =>
            this.ratingRepository.save(rating));
        let savedRatings: Rating[];
        try {
            savedRatings = await Promise.all(promiseRatings);
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }

        let translationEntities: Translation[];
        try {
            translationEntities = await newTranslations
                .reduce(async (acc, translation: Translation, i) => {
                    const accumulator = await Promise.resolve(acc);
                    const createTranslation = { ...translation, rating: savedRatings[i] };
                    const createdTranslation = this.translationsRepository.create(createTranslation);
                    const savedTranslation = await this.translationsRepository.save(createdTranslation);
                    accumulator.push(savedTranslation);
                    acc = Promise.resolve(accumulator);
                    return acc;
                }, Promise.resolve([]));
        } catch (error) {
            throw new MultiLingoSystemError(
                error.message,
                HttpStatus.SERVICE_UNAVAILABLE,
            );
        }

        return translationEntities;
    }

}
