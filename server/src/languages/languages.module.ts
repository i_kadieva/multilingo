import { Rating } from './../database/entities/rating.entity';
import { LanguagesService } from './languages.service';
import { DatabaseModule } from './../database/database.module';
import { Language } from './../database/entities/language.entity';
import { Module, forwardRef } from '@nestjs/common';
import { LanguagesController } from './languages.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Translation } from './../database/entities/translation.entity';
import { TranslationsModule } from './../translations/translations.module';

@Module({
  imports: [
    DatabaseModule,
    forwardRef(() => TranslationsModule),
    TypeOrmModule.forFeature([Language, Rating, Translation]),
  ],
  controllers: [LanguagesController],
  providers: [LanguagesService],
  exports: [LanguagesService],
})
export class LanguagesModule {}
