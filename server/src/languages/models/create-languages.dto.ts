import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
export class CreateLanguagesDto {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public name: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public code: string;

}
