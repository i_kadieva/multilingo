import { ApiModelProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';
export class UpdateLanguagesDto {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public name: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public code: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsBoolean()
    public isActive: boolean;
}
