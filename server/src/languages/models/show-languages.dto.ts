import { Publish } from './../../common/middleware/transformer/decorators/publish';
import { ApiModelProperty } from '@nestjs/swagger';
export class ShowLanguageDTO {

    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish()
    public name: string;

    @ApiModelProperty()
    @Publish()
    public code: string;

    @ApiModelProperty()
    @Publish()
    public isActive?: boolean;

}
