import {ApiModelProperty} from '@nestjs/swagger';
import {Publish} from '../../common/middleware/transformer/decorators/publish';

export class ShowUielementsDto {

    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish()
    public content: string;

    @ApiModelProperty()
    @Publish()
    public resourceName: string;

    @ApiModelProperty()
    @Publish()
    public isDeleted?: boolean;

    @ApiModelProperty()
    @Publish()
    public languageCode: string;
}
