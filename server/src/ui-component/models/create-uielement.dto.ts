import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateUielementDto {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public content: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public resourceName: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public languageCode: string;

}
