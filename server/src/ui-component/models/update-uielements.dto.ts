import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateUielementsDto {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public content: string;
    //
    // @ApiModelProperty()
    // @IsNotEmpty()
    // @IsString()
    // public resourceName: string;
    //
    // @ApiModelProperty()
    // @IsNotEmpty()
    // @IsString()
    // public languageCode: string;

}
