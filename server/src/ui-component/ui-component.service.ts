import { ArticleTranslationDTO } from './../translations/models/article-translation.dto';
import { CreateTranslationDTO } from './../translations/models/create-translation.dto';
import { TranslationsService } from './../translations/translations.service';
import { GetTranslationDTO } from './../translations/models/get-translation.dto';
import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UIElement } from '../database/entities/ui-element.entity';
import { MultiLingoSystemError } from '../common/middleware';
import { UpdateUielementsDto } from './models/update-uielements.dto';
import { CreateUielementDto } from './models/create-uielement.dto';
import { Translation } from '../database/entities/translation.entity';

@Injectable()
export class UiComponentService {

    public constructor(
        @InjectRepository(UIElement) private readonly uiElementsRepository: Repository<UIElement>,
        @InjectRepository(Translation) private readonly translationRepository: Repository<Translation>,
        private readonly translationService: TranslationsService,
    ) {
    }

    public async getUIElements(languageCode: string): Promise<UIElement[]> {
        let allUIs: UIElement[];
        try {
            allUIs = await this.uiElementsRepository.find();
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        const translatedUIs: UIElement[] = await allUIs
            .reduce(async (acc, uiElement: UIElement) => {
                const accumulator = await Promise.resolve(acc);
                if (uiElement.languageCode !== languageCode) {
                    const criteria: GetTranslationDTO = {
                        originalText: uiElement.content,
                        originalLanguageCode: uiElement.languageCode,
                        targetLanguageCode: languageCode,
                    };
                    const uicontent: ArticleTranslationDTO = await this.translationService.getTranslationForArticle(criteria);
                    uiElement.content = uicontent.translatedText;
                }
                accumulator.push(uiElement);
                acc = Promise.resolve(accumulator);
                return acc;
            }, Promise.resolve([]));

        return translatedUIs;
    }

    public async getUIElementById(uiElementId: string, languageCode: string): Promise<UIElement> {
        let uiElement: UIElement;
        try {
            uiElement = await this.uiElementsRepository.findOne({ id: uiElementId, isDeleted: false });
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        const criteria: GetTranslationDTO = {
            originalText: uiElement.content,
            originalLanguageCode: uiElement.languageCode,
            targetLanguageCode: languageCode,
        };
        const uicontent: ArticleTranslationDTO = await this.translationService.getTranslationForArticle(criteria);
        uiElement.content = uicontent.translatedText;

        return uiElement;
    }

    public async createUIElement(body: CreateUielementDto): Promise<UIElement> {
        let savedUIElement: UIElement;
        try {
            savedUIElement = await this.uiElementsRepository.save(body);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        const criteria: CreateTranslationDTO = {
            originalText: savedUIElement.content,
            originalLanguageCode: savedUIElement.languageCode,
            targetLanguageCode: '',
        };
        await this.translationService.createTranslation(criteria);

        return savedUIElement;
    }

    public async updateUIElement(id: string, body: UpdateUielementsDto): Promise<UIElement> {
        const foundUIElement: UIElement = await this.uiElementsRepository.findOne(id);
        if (foundUIElement === undefined) {
            throw new MultiLingoSystemError(`Such UI element does not exist!`, HttpStatus.BAD_REQUEST);
        }
        foundUIElement.content = body.content;
        let savedUIElement: UIElement;
        try {
            savedUIElement = await this.uiElementsRepository.save(foundUIElement);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        const criteria: CreateTranslationDTO = {
            originalText: savedUIElement.content,
            originalLanguageCode: savedUIElement.languageCode,
            targetLanguageCode: '',
        };
        await this.translationService.createTranslation(criteria);

        return savedUIElement;
    }

    public async deleteUIELement(id: string): Promise<UIElement> {
        const foundUIElement: UIElement = await this.uiElementsRepository.findOne(id);
        if (foundUIElement === undefined) {
            throw new MultiLingoSystemError(`Such UI element does not exist!`, HttpStatus.BAD_REQUEST);
        }
        foundUIElement.isDeleted = !foundUIElement.isDeleted;
        let savedUIElement: UIElement;
        try {
            savedUIElement = await this.uiElementsRepository.save(foundUIElement);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        return savedUIElement;
    }
}
