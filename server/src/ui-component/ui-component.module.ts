import { HttpModule, Module } from '@nestjs/common';
import { UiComponentService } from './ui-component.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UIElement } from '../database/entities/ui-element.entity';
import { TranslationsModule } from '../translations/translations.module';
import { UiComponentController } from './ui-component.controller';
import { Translation } from '../database/entities/translation.entity';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([UIElement, Translation]),
    TranslationsModule,
  ],
  providers: [UiComponentService],
  controllers: [UiComponentController],
})
export class UiComponentModule {}
