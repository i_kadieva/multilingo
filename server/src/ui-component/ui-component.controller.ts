import {
    Body,
    Controller, Delete,
    Get,
    HttpCode,
    HttpStatus, Param,
    Post, Put,
    UseGuards,
    UseInterceptors,
    ValidationPipe,
    Query,
} from '@nestjs/common';
import {ApiBadRequestResponse, ApiBearerAuth, ApiOkResponse} from '@nestjs/swagger';
import {RoleGuard, Roles} from '../common/middleware';
import {RoleType} from '../common/enums/role-type';
import {AuthGuard} from '@nestjs/passport';
import {UiComponentService} from './ui-component.service';
import {UIElement} from '../database/entities/ui-element.entity';
import {AuthGuardWithBlacklisting} from '../common/middleware/guards/blacklist.guard';
import {TransformInterceptor} from '../common/middleware/transformer/interceptors/transform.interceptor';
import {ShowUielementsDto} from './models/show-uielements.dto';
import {CreateUielementDto} from './models/create-uielement.dto';
import {UpdateUielementsDto} from './models/update-uielements.dto';

@ApiBearerAuth()
@Controller('multilingo/ui-component')
export class UiComponentController {

    public constructor(private readonly uiService: UiComponentService) { }

    @Get()
    @UseInterceptors(new TransformInterceptor(ShowUielementsDto))
    @ApiOkResponse({ description: 'Returns all UI-elements in storage!', type: UIElement })
    @ApiBadRequestResponse({ description: 'No UI-elements!' })
    @HttpCode(HttpStatus.OK)
    public async getUIElements(@Query() query): Promise<UIElement[]> {
        return await this.uiService.getUIElements(query.languageCode);
    }

    @Get(':uiElementId')
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowUielementsDto))
    @ApiOkResponse({ description: 'Returns all UI-elements in storage!', type: UIElement })
    @ApiBadRequestResponse({ description: 'No UI-elements!' })
    @HttpCode(HttpStatus.OK)
    public async getUIElementById(
        @Param('uiElementId') uiElementId: string,
        @Query() query,
    ): Promise<UIElement> {
        return await this.uiService.getUIElementById(uiElementId, query.languageCode);
    }

    @Post()
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @HttpCode(HttpStatus.CREATED)
    @UseInterceptors(new TransformInterceptor(ShowUielementsDto))
    @ApiOkResponse({ description: 'An UI element was created!', type: ShowUielementsDto })
    @ApiBadRequestResponse({ description: 'Wrong data input' })
    public async addUIElement(@Body(new ValidationPipe({transform: true, whitelist: true})) uiElement: CreateUielementDto): Promise<UIElement> {
        return await this.uiService.createUIElement(uiElement);
    }

    @Put(':ui-elementId')
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @HttpCode(HttpStatus.OK)
    @UseInterceptors(new TransformInterceptor(ShowUielementsDto))
    @ApiBearerAuth()
    @ApiOkResponse({ description: 'An UI element was updated!', type: ShowUielementsDto })
    @ApiBadRequestResponse({ description: 'UI element not found!' })
    public async updateUIElement(
        @Param('ui-elementId') uielementId: string,
        @Body(new ValidationPipe({transform: true, whitelist: true})) body: UpdateUielementsDto,
    ): Promise<UIElement> {
        return await this.uiService.updateUIElement(uielementId, body);
    }

    @Delete(':ui-elementId')
    @Roles(RoleType.Administrator)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @ApiOkResponse({description: 'An UI element was deleted', type: ShowUielementsDto})
    @ApiBadRequestResponse({description: 'UI element not found!'})
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    public async deleteUIElement(
        @Param('ui-elementId') uielementId: string,
    ): Promise<UIElement> {
        return await this.uiService.deleteUIELement(uielementId);
    }
}
