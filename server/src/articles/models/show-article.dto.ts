import { Publish } from './../../common/middleware/transformer/decorators/publish';
import { ApiModelProperty } from '@nestjs/swagger';
import { ShowVersionDTO } from '../../versions/models/show-vesions.dto';
import { ShowUserDTO } from '../../users/models/show-user.dto';
import { User } from '../../database/entities/user.entity';
import { Version } from '../../database/entities/version.entity';

export class ShowArticleDTO {

    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish()
    public createdOn: Date;

    @ApiModelProperty()
    @Publish()
    public updatedOn: Date;

    @ApiModelProperty()
    @Publish()
    public isDeleted?: boolean;

    @ApiModelProperty()
    @Publish(ShowVersionDTO)
    public versions: Version[];

    @ApiModelProperty()
    @Publish(ShowUserDTO)
    public createdBy: User;

}
