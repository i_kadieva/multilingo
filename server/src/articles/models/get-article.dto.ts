import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsBoolean, IsNumber, IsOptional } from 'class-validator';
import { FindOperator } from 'typeorm';

export class GetArticleDTO {

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public id?: string;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public imgPath?: string;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public title?: string;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    public content?: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    @IsOptional()
    public languageCode?: string;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    public isCurrent?: boolean;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    public isDeleted?: boolean;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsNumber()
    public version?: FindOperator<number>;

}
