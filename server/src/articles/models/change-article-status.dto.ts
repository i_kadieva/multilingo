import { IsBoolean, IsNotEmpty, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ChangeArticleStatusDTO {

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    public isCurrent?: boolean;

    @ApiModelProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    public isDeleted?: boolean;

}
