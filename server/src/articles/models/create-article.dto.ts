import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
export class CreateArticleDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public imgPath: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public title: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public content: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    public languageCode: string;

}
