import {Language} from './../database/entities/language.entity';
import {TranslationsModule} from './../translations/translations.module';
import {UsersModule} from './../users/users.module';
import {Version} from './../database/entities/version.entity';
import {Article} from './../database/entities/article.entity';
import {ArticlesController} from './articles.controller';
import {ArticlesService} from './articles.service';
import {DatabaseModule} from './../database/database.module';
import {HelpperModule} from './../helpper/helpper.module';
import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {LanguagesModule} from './../languages/languages.module';
import {JwtStrategy} from '../auth/strategy/jwt.strategy';
import {JwtModule} from '@nestjs/jwt';
import {ConfigModule} from '../config/config.module';
import {ConfigService} from '../config/config.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Article,
            Language,
            Version,
        ]),
        DatabaseModule,
        HelpperModule,
        LanguagesModule,
        TranslationsModule,
        UsersModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.jwtSecret,
                signOptions: {
                    expiresIn: configService.jwtExpireTime,
                },
            }),
        }),
    ],
    providers: [ArticlesService, JwtStrategy],
    controllers: [ArticlesController],
})
export class ArticlesModule {
}
