import { ArticleTranslationDTO } from './../translations/models/article-translation.dto';
import { GetArticleDTO } from './models/get-article.dto';
import { RoleType } from './../common/enums/role-type';
import { CreateTranslationDTO } from './../translations/models/create-translation.dto';
import { TranslationsService } from './../translations/translations.service';
import { GetTranslationDTO } from './../translations/models/get-translation.dto';
import { ChangeArticleStatusDTO } from './models/change-article-status.dto';
import { UsersService } from './../users/users.service';
import { CreateArticleDTO } from './models/create-article.dto';
import { Version } from './../database/entities/version.entity';
import { Article } from './../database/entities/article.entity';
import { HelpperService } from './../helpper/helpper.service';
import { IPayload } from './../auth/models/IPayload';
import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MultiLingoSystemError } from './../common/middleware/exceptions/multiLingo-system.error';
import { Repository } from 'typeorm';
import { Role } from './../database/entities/role.entity';
import { Language } from './../database/entities/language.entity';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '../config/config.service';

@Injectable()
export class ArticlesService {

    public constructor(
        @InjectRepository(Article) private readonly articleRepository: Repository<Article>,
        @InjectRepository(Language) private readonly languageRepository: Repository<Language>,
        @InjectRepository(Version) private readonly versionRepository: Repository<Version>,
        private readonly helpperService: HelpperService,
        private readonly translationService: TranslationsService,
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
    ) { }

    public async getAllArticles(languageCode: string, decode: any): Promise<Article[]> {
        decode = decode.replace('Bearer ', '');
        const user: any = this.jwtService.decode(decode);
        let articlesWithAllVersions: Article[];
        let currentVersionArticles: Article[];
        let foundLanguage: Language;
        try {
            foundLanguage = await this.languageRepository.findOne({ code: languageCode, isActive: true });
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        if (foundLanguage === undefined) {
            languageCode = 'en';
        }
        if (!user) {
            articlesWithAllVersions = await this.getArticles({ isDeleted: false });
        } else {
            if (this.helpperService.isAdmin(user.roles)) {
                articlesWithAllVersions = await this.getArticles({});
            } else {
                const currentVersion: Article[] = await this.getArticles({ isDeleted: false });
                articlesWithAllVersions = currentVersion.filter((article: Article) =>
                (article as any).__createdBy__.id === user.id);
            }
        }
        currentVersionArticles = articlesWithAllVersions
        .map((article: Article) => {
            (article as any).__versions__ = (article as any).__versions__.filter((version: Version) =>
            version.isCurrent === true);
            return article;
        });
        const translatedArticles: Article[] = await currentVersionArticles
            .reduce(async (acc, article: Article) => {
                const accumulator = await Promise.resolve(acc);
                if ((article as any).__versions__[0].languageCode !== languageCode) {
                    const translation = await this.handleTranslation((article as any).__versions__[0], languageCode, false);
                    (article as any).__versions__[0].title = translation[0];
                    (article as any).__versions__[0].content = translation[1];
                }
                accumulator.push(article);
                acc = Promise.resolve(accumulator);
                return acc;
            }, Promise.resolve([]));

        return translatedArticles;
    }

    public async getArticleById(id: string, languageCode: string, decode: any): Promise<Article> {
        decode = decode.replace('Bearer ', '');
        const user: any = this.jwtService.decode(decode);
        let article: Article;
        let allVersions: Article;
        let foundLanguage: Language;
        try {
            foundLanguage = await this.languageRepository.findOne({ code: languageCode, isActive: true });
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        if (foundLanguage === undefined) {
            languageCode = 'en';
        }
        if (!user) {
            allVersions = await this.getArticle({ isDeleted: false, id });
        } else {
            if (this.helpperService.isAdmin(user.roles)) {
                allVersions = await this.getArticle({ id });
            } else {
                allVersions = await this.getArticle({ isDeleted: false, id });
            }
        }
        article = allVersions;
        (article as any).__versions__ = (allVersions as any).__versions__
            .filter((ver: Version) => ver.isCurrent === true);
        if ((article as any).__versions__[0].languageCode !== languageCode) {
            const translation = await this.handleTranslation((article as any).__versions__[0], languageCode, false);
            (article as any).__versions__[0].title = translation[0];
            (article as any).__versions__[0].content = translation[1];
        }

        return article;
    }

    public async getVersions(articleId: string, user: IPayload): Promise<Version[]> {
        let article: Article;
        let versions: Version[] = [];
        try {
            article = await this.getArticle({ id: articleId });
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }

        const roles: string[] = user.roles.map((role: Role) => RoleType[role.name]);
        if (!this.helpperService.isAdmin(roles)) {
            versions = (article as any).__versions__;
        } else {
            versions = (article as any).__versions__
                .filter((version: Version) => version.isDeleted === false);
        }

        return versions;
    }

    public async getVersionById(
        versionId: string,
        withDeleted: boolean,
        language: string,
    ): Promise<Version> {
        let version: Version;
        try {
            if (withDeleted) {
                const where = { id: versionId, isDeleted: withDeleted };
                version = await this.versionRepository.findOne({ where });
            } else {
                version = await this.versionRepository.findOne({ id: versionId });
            }
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }

        if (language) {
            const translatedArticle: ArticleTranslationDTO[] = await this.handleTranslation(version, language, withDeleted);
            (version as any).__versions__[0].title = translatedArticle[0];
            (version as any).__versions__[0].content = translatedArticle[1];
        }

        return version;
    }

    public async createArticle(article: CreateArticleDTO, user: IPayload): Promise<Article> {
        const newArticle: Article = this.articleRepository.create();

        const foundUser = await this.usersService.getUserById(user.id, false);
        if (foundUser === undefined) {
            throw new MultiLingoSystemError(`Such user does not exist!`, HttpStatus.BAD_REQUEST);
        }
        newArticle.createdBy = Promise.resolve(foundUser);
        newArticle.languageCode = article.languageCode;
        let savedArticle: Article;
        try {
            savedArticle = await this.articleRepository.save(newArticle);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        const fullArticle: Article = await this.createVersion(article, savedArticle.id, user);

        return fullArticle;
    }

    public async createVersion(article: CreateArticleDTO, articleId: string, user: IPayload): Promise<Article> {
        if (user === undefined) {
            throw new MultiLingoSystemError(`You don't have permision to create or edit article!`, HttpStatus.BAD_REQUEST);
        }
        const foundUser = await this.usersService.getUserById(user.id, false);
        if (foundUser === undefined) {
            throw new MultiLingoSystemError(`Such user does not exist!`, HttpStatus.BAD_REQUEST);
        }

        const newVersion: Version = this.versionRepository.create(article);

        const foundArticle: Article = await this.getArticle({ isDeleted: false, id: articleId });
        if (foundArticle === undefined) {
            throw new MultiLingoSystemError(`Such article does not exist!`, HttpStatus.BAD_REQUEST);
        }
        const versions = await this.changeStatus(foundArticle, { isCurrent: false });
        newVersion.version = versions.length;

        let savedVersion: Version;
        try {
            savedVersion = await this.versionRepository.save(newVersion);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        versions.push(savedVersion);
        foundArticle.versions = Promise.resolve(versions);
        let fullArticle: Article;
        try {
            fullArticle = await this.articleRepository.save(foundArticle);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }

        const translateTitle: CreateTranslationDTO = {
            originalText: savedVersion.title,
            originalLanguageCode: savedVersion.languageCode,
            targetLanguageCode: '',
        };
        await this.translationService.createTranslation(translateTitle);
        const translateContent: CreateTranslationDTO = {
            originalText: savedVersion.content,
            originalLanguageCode: savedVersion.languageCode,
            targetLanguageCode: '',
        };
        await this.translationService.createTranslation(translateContent);

        return fullArticle;
    }

    public async setCurrentStatus(articleId: string, versionId: string): Promise<Version[]> {
        const foundArticle: Article = await this.getArticle({ id: articleId });
        if (foundArticle === undefined) {
            throw new MultiLingoSystemError(`Such article does not exist!`, HttpStatus.BAD_REQUEST);
        }
        let versions: Version[];
        let currentVersionIndex: number;
        try {
            versions = await this.changeStatus(foundArticle, { isCurrent: false });
            currentVersionIndex = versions.findIndex((version: Version) => version.id === versionId);
            versions[currentVersionIndex] = await this.versionRepository
                .save({ ...versions[currentVersionIndex], isCurrent: true });
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }

        return versions;
    }

    public async deleteArticle(userId: string, articleId: string): Promise<Article> {
        const foundUser = await this.usersService.getUserById(userId, false);
        if (foundUser === undefined) {
            throw new MultiLingoSystemError(`Such user does not exist!`, HttpStatus.BAD_REQUEST);
        }

        const foundArticle: Article = await this.getArticle({ isDeleted: false, id: articleId });
        if (foundArticle === undefined) {
            throw new MultiLingoSystemError(`Such article does not exist!`, HttpStatus.BAD_REQUEST);
        }

        if (foundArticle.isDeleted) {
            throw new MultiLingoSystemError(`Such article does not exists!`, HttpStatus.BAD_REQUEST);
        }

        const isAuthor = (foundArticle as any).__createdBy__.id === userId;
        if (!this.helpperService.isAdmin && !isAuthor) {
            throw new MultiLingoSystemError(`You have not written this article!`, HttpStatus.BAD_REQUEST);
        }
        foundArticle.isDeleted = true;
        await this.changeStatus(foundArticle, { isDeleted: true });

        return await this.articleRepository.save(foundArticle);
    }

    public async getArticles(where: GetArticleDTO): Promise<Article[]> {
        let articles: Article[];
        try {
            articles = await this.articleRepository.find({
                where,
                relations: ['versions', 'createdBy'],
                order: {
                    updatedOn: 'DESC',
                },
            });
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }

        return articles;
    }

    public async getArticle(where: GetArticleDTO): Promise<Article> {
        let article: Article;
        try {
            article = await this.articleRepository.findOne({
                where,
                relations: ['versions', 'createdBy'],
            });

        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        return article;
    }

    public async handleTranslation(
        version: Version, language: string, withDeleted: boolean,
    ): Promise<ArticleTranslationDTO[]> {
        const content: ArticleTranslationDTO[] = [];
        const titleCriteria: GetTranslationDTO = {
            originalText: version.title,
            originalLanguageCode: version.languageCode,
            targetLanguageCode: language,
            isDeleted: withDeleted,
        };

        content[0] = await this.translationService.getTranslationForArticle(titleCriteria);
        const contentCriteria: GetTranslationDTO = { ...titleCriteria, originalText: version.content };
        content[1] = await this.translationService.getTranslationForArticle(contentCriteria);

        return content;
    }

    private async changeStatus(article: Article, status: ChangeArticleStatusDTO): Promise<Version[]> {
        const newVersions = (article as any).__versions__.map((version: Version) =>
            ({ ...version, ...status }));
        let oldVersions: Version[];
        try {
            const vPromise = newVersions.map((version: Version) => this.versionRepository.save(version));
            oldVersions = await Promise.all(vPromise);
        } catch (error) {
            throw new MultiLingoSystemError(error.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
        return oldVersions;
    }

}
