import { ShowVersionDTO } from './../versions/models/show-vesions.dto';
import { USER } from './../common/middleware/decorators/user-decorator';
import { AuthGuardWithBlacklisting } from './../common/middleware/guards/blacklist.guard';
import { RoleGuard } from './../common/middleware/guards/admin.guard';
import { RoleType } from './../common/enums/role-type';
import { Roles } from './../common/middleware/decorators/roles-decorator';
import { TransformInterceptor } from './../common/middleware/transformer/interceptors/transform.interceptor';
import { Article } from './../database/entities/article.entity';
import { ApiUseTags, ApiOkResponse, ApiBadRequestResponse, ApiBearerAuth } from '@nestjs/swagger';
import { ArticlesService } from './articles.service';
import {
    Controller,
    Get,
    UseInterceptors,
    HttpCode,
    HttpStatus,
    Param,
    Post,
    UseGuards,
    Body,
    ValidationPipe,
    Put,
    Delete,
    Query,
} from '@nestjs/common';
import { IPayload } from './../auth/models/IPayload';
import { ShowArticleDTO } from './models/show-article.dto';
import { AuthGuard } from '@nestjs/passport';
import { CreateArticleDTO } from './models/create-article.dto';
import { Version } from '../database/entities/version.entity';
import { Token } from '../common/middleware/decorators/token.decorator';

@ApiUseTags('articles')
@Controller('multilingo/articles')
export class ArticlesController {

    public constructor( private readonly articlesService: ArticlesService ) {}

    @Get()
    @UseInterceptors(new TransformInterceptor(ShowArticleDTO))
    @ApiOkResponse({ description: 'Returns all articles in storage!', type: Article })
    @ApiBadRequestResponse({ description: 'No articles!' })
    @HttpCode(HttpStatus.OK)
    public async getAllArticles(
        @Query() query,
        @Token() user): Promise<Article[]> {
        return await this.articlesService.getAllArticles(query.language, user);
    }

    @Get(':articleId')
    @UseInterceptors(new TransformInterceptor(ShowArticleDTO))
    @ApiOkResponse({ description: 'Returns article by its id!', type: Article })
    @ApiBadRequestResponse({ description: 'No such article!' })
    @HttpCode(HttpStatus.OK)
    public async getArticleById(
        @Param('articleId') articleId: string,
        @Query() query,
        @Token() user,
    ): Promise<Article> {

        return await this.articlesService.getArticleById(articleId, query.language, user);
    }

    @Get(':articleId/versions')
    @HttpCode(HttpStatus.CREATED)
    @Roles(RoleType.Administrator, RoleType.Contributor)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowVersionDTO))
    @ApiBearerAuth()
    @ApiOkResponse({ description: 'Returns all article vertions', type: ShowVersionDTO })
    @ApiBadRequestResponse({ description: 'No versions!' })
    public async getAllVersions(
        @Param('articleId') articleId: string,
        @USER() user: IPayload,
    ): Promise<Version[]> {
        return await this.articlesService.getVersions(articleId, user);
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    @Roles(RoleType.Administrator, RoleType.Contributor)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowArticleDTO))
    @ApiBearerAuth()
    @ApiOkResponse({ description: 'An article was created!', type: CreateArticleDTO })
    @ApiBadRequestResponse({ description: 'Wrong data input' })
    public async createArticle(
        @Body(new ValidationPipe({transform: true, whitelist: true})) article: CreateArticleDTO,
        @USER() user: IPayload,
    ): Promise<Article> {
        return await this.articlesService.createArticle(article, user);
    }

    @Post(':articleId/versions')
    @HttpCode(HttpStatus.CREATED)
    @Roles(RoleType.Administrator, RoleType.Contributor)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowArticleDTO))
    @ApiBearerAuth()
    @ApiOkResponse({ description: 'An article was updated!', type: ShowArticleDTO })
    @ApiBadRequestResponse({ description: 'Wrong data input' })
    public async createVersion(
        @Param('articleId') articleId: string,
        @Body(new ValidationPipe({transform: true, whitelist: true})) article: CreateArticleDTO,
        @USER() user: IPayload,
    ): Promise<Article> {
        return await this.articlesService.createVersion(article, articleId, user);
    }

    @Put(':articleId/versions/:versionId')
    @HttpCode(HttpStatus.OK)
    @Roles(RoleType.Administrator, RoleType.Contributor)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @UseInterceptors(new TransformInterceptor(ShowVersionDTO))
    @ApiBearerAuth()
    @ApiOkResponse({ description: 'An article was updated!', type: ShowVersionDTO })
    @ApiBadRequestResponse({ description: 'Could not set current!' })
    public async setCurrentStatus(
        @Param('articleId') articleId: string,
        @Param('versionId') versionId: string,
    ): Promise<Version[]> {
        return await this.articlesService.setCurrentStatus(articleId, versionId);
    }

    @Delete(':articleId')
    @Roles(RoleType.Administrator, RoleType.Contributor)
    @UseGuards(AuthGuard('jwt'), RoleGuard, AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ShowArticleDTO))
    @ApiOkResponse({description: 'An article was deleted by user/admin', type: ShowArticleDTO})
    @ApiBadRequestResponse({description: 'The article was not deleted!'})
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    public async deleteArticle(
      @USER() user: IPayload,
      @Param('articleId') articleId: string,
    ): Promise<Article> {
        return await this.articlesService.deleteArticle(user.id, articleId);
    }

}
