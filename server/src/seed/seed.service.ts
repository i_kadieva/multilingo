import {TranslationsService} from './../translations/translations.service';
import {HttpService, Injectable, OnApplicationBootstrap, OnApplicationShutdown} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {UIElement} from '../database/entities/ui-element.entity';
import {url} from '../common/url/url';
import {Translation} from '../database/entities/translation.entity';
import {UIElements} from '../common/ui-elements-content/ui-elements';
import {articleArrayForAdmin, articleArrayForUser} from '../common/mock-articles/article-content';

// You can use this service to stop server and add ui-elements into the UIElements array you want to seed.
// If you uncomment the methods when you run 'npm run start' you will fill the data base automatically with articles and ui-elements
@Injectable()
export class SeedService implements OnApplicationBootstrap, OnApplicationShutdown {

    public constructor(
        @InjectRepository(UIElement) private readonly uiElementsRepository: Repository<UIElement>,
        @InjectRepository(Translation) private readonly translationRepository: Repository<Translation>,
        private readonly translationService: TranslationsService,
        private http: HttpService,
    ) {
    }

    async onApplicationShutdown(signal?: string) {
        // This method deletes all ui-elements when you shutdown the server.
        // console.log('Delete UI-elements');
        // for (let el of UIElements) {
        //     await this.uiElementsRepository.delete({ resourceName: el.resourceName });
        //     await this.translationRepository.delete({originalText: el.content});
        // }
    }

    onApplicationBootstrap(): any {
        // You can use this methods if you want to seed ui-elements and articles on npm run start:dev
        // console.log('Seed UI-Elements');
        // this.seedDataBase();
    }

    private async seedDataBase() {
        try {
            for (const element of UIElements) {
                this.http.post(`${url}/ui-component`, element).subscribe();
            }
            await this.http.post(`${url}/session`,
                {usernameOrEmail: 'Admin', password: '123456789a'},
                {headers: {'Content-Type': 'application/json'}})
                .subscribe(async (data) => {
                    // tslint:disable-next-line: prefer-const
                    for (let article of articleArrayForAdmin) {
                        await this.http.post(`${url}/articles`,
                            article,
                            {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + data.data.token,
                                },
                            });
                    }
                });
            await this.http.post(`${url}/session`,
                {usernameOrEmail: 'Admin', password: '123456789a'},
                {headers: {'Content-Type': 'application/json'}})
                .subscribe(async (data) => {
                    // tslint:disable-next-line: prefer-const
                    for (let article of UIElements) {
                        await this.http.post(`${url}/ui-component`,
                            article,
                            {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + data.data.token,
                                },
                            });
                    }
                });
            await this.http.post(`${url}/session`,
                {usernameOrEmail: 'Georgi', password: '123456789a'},
                {headers: {'Content-Type': 'application/json'}})
                .subscribe(async (data) => {
                    // tslint:disable-next-line: prefer-const
                    for (let article of articleArrayForUser) {
                        await this.http.post(`${url}/articles`,
                            article,
                            {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + data.data.token,
                                },
                            });
                    }
                });
        } catch (e) {
            // tslint:disable-next-line: no-console
            console.log(e);
        }
    }
}
