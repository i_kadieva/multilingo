import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UIElement } from '../database/entities/ui-element.entity';
import { TranslationsModule } from '../translations/translations.module';
import { Translation } from '../database/entities/translation.entity';
import { SeedService } from './seed.service';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([UIElement, Translation]),
    TranslationsModule,
  ],
  providers: [SeedService],
})
export class SeedModule {}
