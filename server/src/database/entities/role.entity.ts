import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { RoleType } from './../../common/enums/role-type';

@Entity('roles')
export class Role {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'enum', enum: RoleType, default: RoleType.Contributor, nullable: false })
    public name: RoleType;

}
