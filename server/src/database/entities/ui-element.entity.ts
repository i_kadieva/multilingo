import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('ui_elements')
export class UIElement {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'nvarchar', nullable: false })
    public content: string;

    @Column({ type: 'nvarchar', nullable: false })
    public resourceName: string;

    @Column({ type: 'boolean', nullable: false, default: false })
    public isDeleted: boolean;

    @Column({ type: 'nvarchar', nullable: false })
    public languageCode: string;

}
