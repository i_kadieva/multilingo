import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    ManyToOne,
    UpdateDateColumn,
    OneToMany,
} from 'typeorm';
import { Version } from './version.entity';
import { User } from './user.entity';

@Entity('articles')
export class Article {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'nvarchar', nullable: false })
    public languageCode: string;

    @CreateDateColumn({ nullable: false })
    public createdOn: Date;

    @UpdateDateColumn()
    public updatedOn: Date;

    @Column({ type: 'boolean', nullable: false, default: false })
    public isDeleted: boolean;

    @OneToMany(type => Version, version => version.article)
    public versions: Promise<Version[]>;

    @ManyToOne(type => User, user => user.articles)
    public createdBy: Promise<User>;

}
