import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('blacklist')
export class Blacklist {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'longtext', nullable: false })
    public token: string;

}
