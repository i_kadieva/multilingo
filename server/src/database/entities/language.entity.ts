import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('languages')
export class Language {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'nvarchar', unique: true })
    public name: string;

    @Column({ type: 'nvarchar', unique: true})
    public code: string;

    @Column({ type: 'boolean', nullable: false, default: true })
    public isActive: boolean;

}
