import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('ratings')
export class Rating {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'integer', default: 0 })
    public voted: number;

    @Column({ type: 'integer', default: 0 })
    public rating: number;

    @Column({ type: 'dec', default: 0 })
    public avgrating: number;

}
