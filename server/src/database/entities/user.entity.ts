import { Article } from './article.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';
import { Role } from './role.entity';

@Entity('users')
export class User {

  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'nvarchar', nullable: false, unique: true, length: 50 })
  public username: string;

  @Column({ type: 'nvarchar', length: 50, nullable: false, unique: true})
  public email: string;

  @Column({ type: 'nvarchar', nullable: false })
  public password: string;

  @Column({ type: 'nvarchar', nullable: true })
  public avatarPath: string;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isDeleted: boolean;

  @OneToMany(type => Article, article => article.createdBy)
  public articles: Promise<Article[]>;

  @Column({ type: 'nvarchar', nullable: false })
  public preferedLangCode: string;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable()
  public roles: Role[];

}
