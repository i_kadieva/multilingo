import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
} from 'typeorm';
import { Article } from './article.entity';

@Entity('versions')
export class Version {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'text', nullable: false })
    public imgPath: string;

    @Column({ type: 'nvarchar', nullable: false })
    public title: string;

    @Column({ type: 'text', nullable: false })
    public content: string;

    @Column({ type: 'int', nullable: false })
    public version: number;

    @Column({ type: 'boolean', nullable: false, default: true })
    public isCurrent: boolean;

    @Column({ type: 'boolean', nullable: false, default: false })
    public isDeleted: boolean;

    @Column({ type: 'nvarchar', nullable: false })
    public languageCode: string;

    @ManyToOne(type => Article, article => article.versions)
    public article: Promise<Article>;

}
