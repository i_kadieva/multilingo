import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Rating } from './rating.entity';

@Entity('translations')
export class Translation {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'text', nullable: false })
    public originalText: string;

    @Column({ type: 'text', nullable: false })
    public translation: string;

    @Column({ type: 'boolean', nullable: false, default: false })
    public isDeleted: boolean;

    @Column({ type: 'nvarchar', nullable: false })
    public originalLanguageCode: string;

    @Column({ type: 'nvarchar', nullable: false })
    public targetLanguageCode: string;

    @OneToOne(type => Rating, {eager: true})
    @JoinColumn()
    public rating: Rating;

}
