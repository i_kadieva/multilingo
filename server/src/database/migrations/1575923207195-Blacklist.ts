import {MigrationInterface, QueryRunner} from 'typeorm';

export class Blacklist1575923207195 implements MigrationInterface {
    name = 'Blacklist1575923207195';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `roles` CHANGE `name` `name` enum (\'Contributor\', \'Editor\', \'Administrator\') NOT NULL DEFAULT \'Contributor\'', undefined);
        await queryRunner.query('ALTER TABLE `languages` ADD UNIQUE INDEX `IDX_7397752718d1c9eb873722ec9b` (`code`)', undefined);
        await queryRunner.query('ALTER TABLE `ratings` CHANGE `avgrating` `avgrating` decimal NOT NULL DEFAULT 0', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `ratings` CHANGE `avgrating` `avgrating` decimal(10,0) NOT NULL DEFAULT \'0\'', undefined);
        await queryRunner.query('ALTER TABLE `languages` DROP INDEX `IDX_7397752718d1c9eb873722ec9b`', undefined);
        await queryRunner.query('ALTER TABLE `roles` CHANGE `name` `name` enum (\'0\', \'1\', \'2\') NOT NULL DEFAULT \'0\'', undefined);
    }

}
