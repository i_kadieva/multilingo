import {MigrationInterface, QueryRunner} from 'typeorm';

export class TokenLongText1576222101888 implements MigrationInterface {
    name = 'TokenLongText1576222101888';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `versions` DROP COLUMN `imgPath`', undefined);
        await queryRunner.query('ALTER TABLE `versions` ADD `imgPath` text NOT NULL', undefined);
        await queryRunner.query('ALTER TABLE `ratings` CHANGE `avgrating` `avgrating` decimal NOT NULL DEFAULT 0', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `ratings` CHANGE `avgrating` `avgrating` decimal(10,0) NOT NULL DEFAULT \'0\'', undefined);
        await queryRunner.query('ALTER TABLE `versions` DROP COLUMN `imgPath`', undefined);
        await queryRunner.query('ALTER TABLE `versions` ADD `imgPath` varchar(255) NOT NULL', undefined);
    }

}
