import {MigrationInterface, QueryRunner} from 'typeorm';

export class ArticleLanguage1574797266507 implements MigrationInterface {
    name = 'ArticleLanguage1574797266507';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `articles` ADD `languageCode` varchar(255) NOT NULL', undefined);
        await queryRunner.query('ALTER TABLE `ratings` CHANGE `avgrating` `avgrating` decimal NOT NULL DEFAULT 0', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `ratings` CHANGE `avgrating` `avgrating` decimal(10,0) NOT NULL DEFAULT \'0\'', undefined);
        await queryRunner.query('ALTER TABLE `articles` DROP COLUMN `languageCode`', undefined);
    }

}
