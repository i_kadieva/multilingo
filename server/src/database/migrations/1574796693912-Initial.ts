import {MigrationInterface, QueryRunner} from 'typeorm';

export class Initial1574796693912 implements MigrationInterface {
    name = 'Initial1574796693912';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE TABLE `versions` (`id` varchar(36) NOT NULL, `imgPath` varchar(255) NOT NULL, `title` varchar(255) NOT NULL, `content` text NOT NULL, `version` int NOT NULL, `isCurrent` tinyint NOT NULL DEFAULT 1, `isDeleted` tinyint NOT NULL DEFAULT 0, `languageCode` varchar(255) NOT NULL, `articleId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `roles` (`id` varchar(36) NOT NULL, `name` enum (\'0\', \'1\', \'2\') NOT NULL DEFAULT \'0\', PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `users` (`id` varchar(36) NOT NULL, `username` varchar(50) NOT NULL, `email` varchar(50) NOT NULL, `password` varchar(255) NOT NULL, `avatarPath` varchar(255) NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `preferedLangCode` varchar(255) NOT NULL, UNIQUE INDEX `IDX_fe0bb3f6520ee0469504521e71` (`username`), UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `articles` (`id` varchar(36) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `createdById` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `blacklist` (`id` varchar(36) NOT NULL, `token` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `languages` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `code` varchar(255) NOT NULL, `isActive` tinyint NOT NULL DEFAULT 1, UNIQUE INDEX `IDX_9c0e155475f0aa782e4a617896` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `ratings` (`id` varchar(36) NOT NULL, `voted` int NOT NULL DEFAULT 0, `rating` int NOT NULL DEFAULT 0, `avgrating` decimal NOT NULL DEFAULT 0, PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `translations` (`id` varchar(36) NOT NULL, `originalText` text NOT NULL, `translation` text NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `originalLanguageCode` varchar(255) NOT NULL, `targetLanguageCode` varchar(255) NOT NULL, `ratingId` varchar(36) NULL, UNIQUE INDEX `REL_053f8f7e4a8fd6f93351a2e6a4` (`ratingId`), PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `ui_elements` (`id` varchar(36) NOT NULL, `content` varchar(255) NOT NULL, `resourceName` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `languageCode` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('CREATE TABLE `users_roles_roles` (`usersId` varchar(36) NOT NULL, `rolesId` varchar(36) NOT NULL, INDEX `IDX_df951a64f09865171d2d7a502b` (`usersId`), INDEX `IDX_b2f0366aa9349789527e0c36d9` (`rolesId`), PRIMARY KEY (`usersId`, `rolesId`)) ENGINE=InnoDB', undefined);
        await queryRunner.query('ALTER TABLE `versions` ADD CONSTRAINT `FK_1bc1cffda4f22a6842f2b8d6922` FOREIGN KEY (`articleId`) REFERENCES `articles`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION', undefined);
        await queryRunner.query('ALTER TABLE `articles` ADD CONSTRAINT `FK_090b4acad1cc10daa2002367431` FOREIGN KEY (`createdById`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION', undefined);
        await queryRunner.query('ALTER TABLE `translations` ADD CONSTRAINT `FK_053f8f7e4a8fd6f93351a2e6a4f` FOREIGN KEY (`ratingId`) REFERENCES `ratings`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION', undefined);
        await queryRunner.query('ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_df951a64f09865171d2d7a502b1` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
        await queryRunner.query('ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_b2f0366aa9349789527e0c36d97` FOREIGN KEY (`rolesId`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_b2f0366aa9349789527e0c36d97`', undefined);
        await queryRunner.query('ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_df951a64f09865171d2d7a502b1`', undefined);
        await queryRunner.query('ALTER TABLE `translations` DROP FOREIGN KEY `FK_053f8f7e4a8fd6f93351a2e6a4f`', undefined);
        await queryRunner.query('ALTER TABLE `articles` DROP FOREIGN KEY `FK_090b4acad1cc10daa2002367431`', undefined);
        await queryRunner.query('ALTER TABLE `versions` DROP FOREIGN KEY `FK_1bc1cffda4f22a6842f2b8d6922`', undefined);
        await queryRunner.query('DROP INDEX `IDX_b2f0366aa9349789527e0c36d9` ON `users_roles_roles`', undefined);
        await queryRunner.query('DROP INDEX `IDX_df951a64f09865171d2d7a502b` ON `users_roles_roles`', undefined);
        await queryRunner.query('DROP TABLE `users_roles_roles`', undefined);
        await queryRunner.query('DROP TABLE `ui_elements`', undefined);
        await queryRunner.query('DROP INDEX `REL_053f8f7e4a8fd6f93351a2e6a4` ON `translations`', undefined);
        await queryRunner.query('DROP TABLE `translations`', undefined);
        await queryRunner.query('DROP TABLE `ratings`', undefined);
        await queryRunner.query('DROP INDEX `IDX_9c0e155475f0aa782e4a617896` ON `languages`', undefined);
        await queryRunner.query('DROP TABLE `languages`', undefined);
        await queryRunner.query('DROP TABLE `blacklist`', undefined);
        await queryRunner.query('DROP TABLE `articles`', undefined);
        await queryRunner.query('DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`', undefined);
        await queryRunner.query('DROP INDEX `IDX_fe0bb3f6520ee0469504521e71` ON `users`', undefined);
        await queryRunner.query('DROP TABLE `users`', undefined);
        await queryRunner.query('DROP TABLE `roles`', undefined);
        await queryRunner.query('DROP TABLE `versions`', undefined);
    }

}
