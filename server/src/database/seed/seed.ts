import * as bcrypt from 'bcrypt';
import {createConnection} from 'typeorm';
import {Role} from '../entities/role.entity';
import {User} from '../entities/user.entity';
import {RoleType} from '../../common/enums/role-type';
import {Language} from '../entities/language.entity';
import {UiComponentService} from '../../ui-component/ui-component.service';
import {UIElement} from '../entities/ui-element.entity';

const seed = async () => {

    const connection = await createConnection();
    const userRepository = connection.getRepository(User);
    const roleRepository = connection.getRepository(Role);
    const languageRepository = connection.getRepository(Language);
    const uiElementsRepository = connection.getRepository(UIElement);

    const english = await languageRepository.save({
       name: 'English',
       code: 'en',
    });

    const french = await languageRepository.save({
        name: 'Français',
        code: 'fr',
    });

    const spanish = await languageRepository.save({
        name: 'Español',
        code: 'es',
    });

    const german = await languageRepository.save({
        name: 'Deutsch',
        code: 'de',
    });

    const bulgarian = await languageRepository.save({
        name: 'Български',
        code: 'bg',
    });

    const russian = await languageRepository.save({
        name: 'Русский',
        code: 'ru',
    });

    // Creating user roles
    const admin = await roleRepository.save({
        name: RoleType.Administrator,
    });
    const user = await roleRepository.save({
        name: RoleType.Contributor,
    });
    const editor = await roleRepository.save({
        name: RoleType.Editor,
    });

    // Creating first admin
    const firstAdmin = new User();
    firstAdmin.username = 'Admin';
    firstAdmin.email = 'admin@abv.bg';
    firstAdmin.roles = [admin, user, editor];
    firstAdmin.password = await bcrypt.hash('123456789a', 10);
    firstAdmin.avatarPath = 'https://d29jd5m3t61t9.cloudfront.net/static/images/comprofiler/gallery/operator/operator_m.png';
    firstAdmin.preferedLangCode = 'en';

    await userRepository.save(firstAdmin);

    const firstEditor = new User();
    firstEditor.username = 'Editor';
    firstEditor.email = 'editor@abv.bg';
    firstEditor.roles = [editor];
    firstEditor.password = await bcrypt.hash('123456789a', 10);
    firstEditor.avatarPath = 'https://cdn0.iconfinder.com/data/icons/users-groups-1/512/user_edit-512.png';
    firstEditor.preferedLangCode = 'es';

    await userRepository.save(firstEditor);

    const firstUser = new User();
    firstUser.username = 'Georgi';
    firstUser.email = 'georgi@abv.bg';
    firstUser.roles = [user];
    firstUser.password = await bcrypt.hash('123456789a', 10);
    firstUser.avatarPath = 'https://f0.pngfuel.com/png/312/283/man-face-clip-art-png-clip-art-thumbnail.png';
    firstUser.preferedLangCode = 'bg';

    await userRepository.save(firstUser);

    await connection.close();

    // tslint:disable-next-line: no-console
    console.log(`Data seeded successfully`);
};
seed().then()
    // tslint:disable-next-line: no-console
    .catch(console.log);
