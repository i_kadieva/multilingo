// tslint:disable-next-line:no-var-requires
import {articleArrayForAdmin, articleArrayForUser} from '../../common/mock-articles/article-content';

// tslint:disable-next-line: no-var-requires
const fetch = require('node-fetch');

import {url} from '../../common/url/url';
import {UIElements} from '../../common/ui-elements-content/ui-elements';

const seed = async () => {

    try {
        const token = await fetch(`${url}/session`, {
            method: 'POST',
            body: JSON.stringify({usernameOrEmail: 'Admin', password: '123456789a'}),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        const token1 = await fetch(`${url}/session`, {
            method: 'POST',
            body: JSON.stringify({usernameOrEmail: 'Georgi', password: '123456789a'}),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        const bearer = await token.json();
        const bearer1 = await token1.json();
        for (const article of articleArrayForUser) {
            await fetch(`${url}/articles`, {
                method: 'POST',
                body: JSON.stringify(article),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + bearer1.token,
                },
            });
        }
        for (const article of articleArrayForAdmin) {
            await fetch(`${url}/articles`, {
                method: 'POST',
                body: JSON.stringify(article),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + bearer.token,
                },
            });
        }
        // tslint:disable-next-line: prefer-const
        for (let element of UIElements) {
            await fetch(`${url}/ui-component`, {
                method: 'POST',
                body: JSON.stringify(element),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + bearer.token,
                },
            // tslint:disable-next-line: no-console
            }).then(() => console.log('Success'));
        }
    } catch (e) {
        // tslint:disable-next-line: no-console
        console.log(e);
    }
    // tslint:disable-next-line: no-console
    console.log(`Data seeded successfully`);
};
seed().then()
    // tslint:disable-next-line: no-console
    .catch(console.log);
