import {createConnection} from 'typeorm';
import {User} from '../entities/user.entity';
import {Role} from '../entities/role.entity';
import {Language} from '../entities/language.entity';
import {Article} from '../entities/article.entity';
import {Translation} from '../entities/translation.entity';
import {UIElement} from '../entities/ui-element.entity';
import {Version} from '../entities/version.entity';
import {Rating} from '../entities/rating.entity';
import {UIElements} from '../../common/ui-elements-content/ui-elements';

const main = async () => {
    const connection = await createConnection();
    const userRepository = connection.getRepository(User);
    const roleRepository = connection.getRepository(Role);
    const languageRepository = connection.getRepository(Language);
    const articleRepository = connection.getRepository(Article);
    const translationsRepository = connection.getRepository(Translation);
    const uiElementsRepository = connection.getRepository(UIElement);
    const versionsRepository = connection.getRepository(Version);
    const ratingRepository = connection.getRepository(Rating);

    for (const el of UIElements) {
        await uiElementsRepository.delete({ resourceName: el.resourceName });
        await translationsRepository.delete({originalText: el.content});
    }
    // Clean all data
    // await roleRepository.delete({});
    // await userRepository.delete({});
    // await languageRepository.delete({});
    // await translationsRepository.delete({});
    // await articleRepository.delete({});
    // await versionsRepository.delete({});
    // await ratingRepository.delete({});

    await connection.close();

    // tslint:disable-next-line: no-console
    console.log(`Data cleaned successfully!`);

};

main()
    .catch(console.log);
